<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Plugin\DatasetTransform;

use Drupal\data_pipelines\Attribute\DatasetTransform;
use Drupal\data_pipelines\DatasetData;
use Drupal\data_pipelines\Transform\TransformPluginBase;

/**
 * Defines a transform that maps values.
 */
#[DatasetTransform(
  id: 'map',
  fields: TRUE,
)]
class MapValues extends TransformPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'map' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function doTransformField(string $field_name, DatasetData $record): DatasetData {
    $map = $this->configuration['map'];
    $record = parent::doTransformField($field_name, $record);
    if ($record->offsetExists($field_name)) {
      $value = $record[$field_name];
      if (array_key_exists($value, $map)) {
        $record[$field_name] = $map[$value];
      }
      return $record;
    }
    return $record;
  }

}
