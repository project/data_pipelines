<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Plugin\DatasetTransform;

use Drupal\data_pipelines\Attribute\DatasetTransform;
use Drupal\data_pipelines\DatasetData;
use Drupal\data_pipelines\Transform\TransformPluginBase;

/**
 * Defines a transform that unset records.
 */
#[DatasetTransform(
  id: 'remove',
  records: TRUE,
)]
class Remove extends TransformPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'fields' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function doTransformRecord(DatasetData $record): DatasetData {
    $record = parent::doTransformRecord($record);
    if (!$this->configuration['fields']) {
      return $record;
    }
    foreach ($this->configuration['fields'] as $field_name) {
      if ($record->offsetExists($field_name)) {
        unset($record[$field_name]);
      }
    }
    return $record;
  }

}
