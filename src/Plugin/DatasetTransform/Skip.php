<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Plugin\DatasetTransform;

use Drupal\data_pipelines\Attribute\DatasetTransform;
use Drupal\data_pipelines\DatasetData;
use Drupal\data_pipelines\Exception\TransformSkipRecordException;
use Drupal\data_pipelines\Transform\TransformPluginBase;

/**
 * Defines a transform that skip records.
 */
#[DatasetTransform(
  id: 'skip',
  records: TRUE,
)]
class Skip extends TransformPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return parent::defaultConfiguration() + [
      'field' => '',
      'value' => '',
      'not_equals' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\data_pipelines\Exception\TransformSkipRecordException
   */
  protected function doTransformRecord(DatasetData $record): DatasetData {
    $record = parent::doTransformRecord($record);
    if (!empty($this->configuration['field']) && $record->offsetExists($this->configuration['field'])) {
      $field_name = $this->configuration['field'];
      $field_value_to_compare = $this->configuration['value'];
      $field_value = $record->offsetGet($field_name);
      $skip = $this->configuration['not_equals'] ? ($field_value != $field_value_to_compare) : ($field_value === $field_value_to_compare);
      if ($skip) {
        $message = $this->configuration['not_equals'] ? sprintf('The value of the field "%s" is not equal to the value "%s".', $field_name, $field_value_to_compare)
          : sprintf('The value of the field "%s" is equal to the value "%s".', $field_name, $field_value_to_compare);
        throw new TransformSkipRecordException($message);
      }
    }
    return $record;
  }

}
