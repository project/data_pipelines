<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Plugin\DatasetTransform;

use Drupal\Component\Utility\Random;
use Drupal\data_pipelines\Attribute\DatasetTransform;
use Drupal\data_pipelines\DatasetData;
use Drupal\data_pipelines\Transform\TransformPluginBase;

/**
 * Defines a transform that concatenates records.
 */
#[DatasetTransform(
  id: 'concat',
  records: TRUE,
)]
class Concat extends TransformPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'separator' => ' ',
      'fields' => [],
      'as' => (new Random())->name(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function doTransformRecord(DatasetData $record): DatasetData {
    $as = $this->configuration['as'];
    $record = parent::doTransformRecord($record);
    if (!$this->configuration['fields']) {
      return $record;
    }
    $value = [];
    foreach ($this->configuration['fields'] as $field_name) {
      if ($record->offsetExists($field_name)) {
        $value[] = $record[$field_name];
      }
    }
    $record[$as] = implode($this->configuration['separator'], $value);
    return $record;
  }

}
