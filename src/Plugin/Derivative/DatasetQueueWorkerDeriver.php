<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a deriver for the indexer plugins.
 */
final class DatasetQueueWorkerDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new DatasetQueueWorkerDeriver.
   *
   * @param string $base_plugin_id
   *   The base plugin ID.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct($base_plugin_id, EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $base_plugin_id,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [];
    $results = $this->entityTypeManager
      ->getStorage('data_pipelines')
      ->getAggregateQuery()
      ->accessCheck(FALSE)
      ->groupBy('name')
      ->groupBy('machine_name')
      ->groupBy('id')
      ->execute();
    if (!$results) {
      return $this->derivatives;
    }
    foreach ($results as $build_details) {
      $this->derivatives[$build_details['machine_name']] = [
        'title' => new TranslatableMarkup('@default for @name', [
          '@default' => $base_plugin_definition['title'],
          '@name' => $build_details['name'],
        ]),
        'data_pipelines_id' => $build_details['id'],
      ] + $base_plugin_definition;
    }
    return $this->derivatives;
  }

}
