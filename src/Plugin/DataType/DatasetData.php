<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\Core\TypedData\MapDataDefinition;
use Drupal\Core\TypedData\Plugin\DataType\Map;
use Drupal\data_pipelines\DatasetData as DatasetDataValueObject;

/**
 * Defines a typed-data type for Dataset data.
 */
#[DataType(
  id: 'data_pipelines_data',
  label: new TranslatableMarkup('Dataset data'),
  definition_class: MapDataDefinition::class,
)]
class DatasetData extends Map {

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    if ($values instanceof DatasetDataValueObject) {
      $values = $values->getArrayCopy();
    }
    parent::setValue($values, $notify);
  }

}
