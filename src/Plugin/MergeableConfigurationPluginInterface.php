<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Plugin;

/**
 * An interface that indicates the configuration for the plugin can be merged.
 */
interface MergeableConfigurationPluginInterface {

  /**
   * A method to merge the configuration.
   *
   * @param array $configuration
   *   The configuration to merge.
   */
  public function mergeConfiguration(array $configuration): void;

}
