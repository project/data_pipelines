<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Plugin\Validation\Constraint;

use Drupal\data_pipelines\Traits\JsonPathTrait;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * A class that acts as the validator for the json path constraint.
 */
class JsonPathConstraintValidator extends ConstraintValidator {

  use JsonPathTrait;

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    assert($constraint instanceof JsonPathConstraint);
    if (!$this->validateJsonPath($value)) {
      $this->context->addViolation($constraint->message, ['@path' => $value]);
    }
  }

}
