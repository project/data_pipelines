<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Plugin\Validation\Constraint;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Validation\Attribute\Constraint;
use Symfony\Component\Validator\Constraint as SymfonyConstraint;

/**
 * A class that acts as a constraint to validate a json path.
 */
#[Constraint(
  id: 'JsonPath',
  label: new TranslatableMarkup('Json path', [], ['context' => 'Validation'])
)]
class JsonPathConstraint extends SymfonyConstraint {

  /**
   * The error message.
   *
   * @var string
   */
  public $message = 'The json path @path is invalid';

}
