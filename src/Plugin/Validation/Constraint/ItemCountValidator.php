<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Defines a validator for ItemCount.
 */
class ItemCountValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    assert($constraint instanceof ItemCount);
    if (count($value) !== $constraint->expectedCount) {
      $this->context->addViolation($constraint->message, ['@count' => $constraint->expectedCount]);
    }
  }

}
