<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Plugin\Validation\Constraint;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Validation\Attribute\Constraint;
use Symfony\Component\Validator\Constraint as SymfonyConstraint;

/**
 * Defines a validation constraint for checking item counts are consistent.
 */
#[Constraint(
  id: 'ItemCount',
  label: new TranslatableMarkup('Item count', [], ['context' => 'Validation'])
)]
class ItemCount extends SymfonyConstraint {

  /**
   * Expected count of records.
   *
   * @var int
   */
  public $expectedCount = 0;

  /**
   * Error message.
   *
   * @var string
   */
  public $message = 'All records should have @count values';

}
