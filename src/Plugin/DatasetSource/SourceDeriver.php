<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Plugin\DatasetSource;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\data_pipelines\Source\Resource\SourceResourceManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A class to derive sources from using their resource counterparts.
 */
class SourceDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The source resource manager.
   *
   * @var \Drupal\data_pipelines\Source\Resource\SourceResourceManager
   */
  protected $sourceResourceManager;

  /**
   * SourceDeriver constructor.
   *
   * @param \Drupal\data_pipelines\Source\Resource\SourceResourceManager $source_resource_manager
   *   The source resource manager.
   */
  final public function __construct(SourceResourceManager $source_resource_manager) {
    $this->sourceResourceManager = $source_resource_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static($container->get('data_pipelines.source_resource.manager'));
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {
    /** @var \Drupal\data_pipelines\Source\Resource\SourceResourceBase $source_resource */
    foreach ($this->sourceResourceManager->getSourceResources() as $source_resource_id => $source_resource_service) {
      $source_resource_label = ucfirst($source_resource_id);
      $this->derivatives[$source_resource_id] = $base_plugin_definition;
      $this->derivatives[$source_resource_id]['label'] = "{$base_plugin_definition['label']} {$source_resource_label}";
      $this->derivatives[$source_resource_id]['description'] = "{$base_plugin_definition['label']} extracted from a {$source_resource_label}";
      $this->derivatives[$source_resource_id]['source_resource_service'] = $source_resource_service;
      $this->derivatives[$source_resource_id]['source_resource_id'] = $source_resource_id;
    }
    return $this->derivatives;
  }

}
