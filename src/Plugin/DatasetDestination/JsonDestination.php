<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Plugin\DatasetDestination;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\data_pipelines\Attribute\DatasetDestination;

/**
 * A class for providing JSON as an output.
 */
#[DatasetDestination(
  id: 'file_json',
  label: new TranslatableMarkup('JSON File'),
  description: new TranslatableMarkup('Writes the dataset to a JSON file.'),
)]
class JsonDestination extends FileDestinationBase {

  /**
   * {@inheritdoc}
   */
  protected function getFileExtension(): string {
    return 'json';
  }

  /**
   * {@inheritdoc}
   */
  protected function serializeChunk(array $chunk): string {
    return (key($chunk) !== 0 ? ",\n" : '') . Json::encode($chunk);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['dir']['#placeholder'] = 'example/';
    return $form;
  }

}
