<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Plugin\DatasetDestination;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\data_pipelines\Attribute\DatasetDestination;
use Drupal\data_pipelines\Destination\DatasetDestinationPluginBase;
use Drupal\data_pipelines\Entity\DatasetInterface;
use Drupal\data_pipelines\Entity\DestinationInterface;

/**
 * A class that provides Null as a destination plugin.
 */
#[DatasetDestination(
  id: 'null',
  label: new TranslatableMarkup('Null'),
  description: new TranslatableMarkup('Does nothing with the dataset.'),
)]
class NullDestination extends DatasetDestinationPluginBase {

  /**
   * {@inheritdoc}
   */
  public function viewSettings(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function saveDataSet(DatasetInterface $dataset, DestinationInterface $destination): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteDataSet(DatasetInterface $dataset, DestinationInterface $destination): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
  }

}
