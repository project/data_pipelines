<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityConstraintViolationListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\data_pipelines\Entity\Dataset;
use Drupal\data_pipelines\Entity\DatasetInterface;

/**
 * Provides a form for adding/editing datasets.
 *
 * @codeCoverageIgnore
 * @see \Drupal\Tests\data_pipelines\Functional\DatasetUiTest
 */
class DatasetForm extends ContentEntityForm {

  /**
   * Flag to track if validation is occurring.
   *
   * @var bool
   */
  protected $isValidating = FALSE;

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $dataset = $this->entity;
    assert($dataset instanceof DatasetInterface);

    if ($this->operation === 'edit') {
      $form['#title'] = $this->t('Edit dataset %label', ['%label' => $dataset->label()]);
    }

    // @todo remove these changes
    //   https://www.drupal.org/project/drupal/issues/2685749 is fixed.
    $form['machine_name'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Machine Name'),
      '#maxlength' => 128,
      '#weight' => -4,
      '#default_value' => $dataset->getMachineName(),
      '#disabled' => !$dataset->isNew(),
      '#machine_name' => [
        'exists' => [Dataset::class, 'loadByMachineName'],
      ],
      '#element_validate' => [],
    ];

    return parent::form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @todo remove this method once
   *   https://www.drupal.org/project/drupal/issues/2685749 is fixed.
   */
  protected function getEditedFieldNames(FormStateInterface $form_state) {
    return array_merge([
      'machine_name',
    ], parent::getEditedFieldNames($form_state));
  }

  /**
   * {@inheritdoc}
   *
   * @todo remove this method once
   *   https://www.drupal.org/project/drupal/issues/2685749 is fixed.
   */
  protected function flagViolations(EntityConstraintViolationListInterface $violations, array $form, FormStateInterface $form_state) {
    // Manually flag violations of fields not handled by the form display. This
    // is necessary as entity form displays only flag violations for fields
    // contained in the display.
    $field_names = [
      'machine_name',
    ];
    foreach ($violations->getByFields($field_names) as $violation) {
      [$field_name] = explode('.', $violation->getPropertyPath(), 2);
      $form_state->setErrorByName($field_name, $violation->getMessage());
    }
    parent::flagViolations($violations, $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['process'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save and process'),
      '#submit' => ['::submitForm', '::saveAndProcess'],
    ];
    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $dataset = $this->entity;
    assert($dataset instanceof DatasetInterface);

    $result = $dataset->save();

    $edit_link = $dataset->toLink($this->t('Edit'), 'edit-form')->toString();
    $view_link = $dataset->toLink()->toString();
    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created dataset %dataset.', [
          '%dataset' => $view_link,
        ]));
        $this->logger('data_pipelines')->notice('Created dataset %dataset.', [
          '%dataset' => $dataset->label(),
          'link' => $edit_link,
        ]);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('Saved dataset %dataset.', [
          '%dataset' => $view_link,
        ]));
        $this->logger('data_pipelines')->notice('Saved dataset %dataset.', [
          '%dataset' => $dataset->label(),
          'link' => $edit_link,
        ]);
        break;
    }
    $form_state->setRedirectUrl($dataset->toUrl('collection'));
    return $result;
  }

  /**
   * Form submission handler for the 'saveAndProcess' action.
   *
   * Saves the dataset and processes immediately.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return int
   *   Either SAVED_NEW or SAVED_UPDATED, depending on the operation performed.
   */
  public function saveAndProcess(array $form, FormStateInterface $form_state): int {
    $result = $this->save($form, $form_state);
    $dataset = $this->entity;
    assert($dataset instanceof DatasetInterface);
    if ($dataset->isPublished()) {
      batch_set(DatasetBatchOperations::batchForDataset($dataset));
      $form_state->setRedirectUrl($dataset->toUrl('collection'));
    }
    else {
      $this->messenger()->addWarning($this->t('Unpublished dataset cannot be indexed.'));
      $form_state->setRedirectUrl($dataset->toUrl('edit-form'));
    }
    return $result;
  }

}
