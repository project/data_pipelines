<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\data_pipelines\Entity\DatasetInterface;

/**
 * Defines a form for processing a dataset.
 *
 * @codeCoverageIgnore
 * @see \Drupal\Tests\data_pipelines\Functional\DatasetUiTest
 */
class DatasetProcessForm extends ContentEntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return new TranslatableMarkup('Are you sure you wish to process this dataset?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('entity.data_pipelines.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $dataset = $this->entity;
    assert($dataset instanceof DatasetInterface);
    if ($dataset->isPublished()) {
      batch_set(DatasetBatchOperations::batchForDataset($dataset));
      $form_state->setRedirectUrl($dataset->toUrl('collection'));
    }
    else {
      $this->messenger()->addWarning($this->t('Unpublished dataset cannot be indexed.'));
      $form_state->setRedirectUrl($dataset->toUrl('process-form'));
    }
  }

}
