<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Form;

/**
 * The context when processing the dataset as part of a batch.
 */
class DatasetBatchContext {

  /**
   * The batch chunk size.
   *
   * @var int
   */
  protected int $chunkSize;

  /**
   * Whether the batch is finished.
   *
   * @var bool
   */
  protected bool $finished;

  /**
   * The number of valid items in the batch.
   *
   * @var int
   */
  protected int $valid;

  /**
   * The number if invalid items in the batch.
   *
   * @var int
   */
  protected int $invalid;

  /**
   * The number of items processed in the batch.
   *
   * @var int
   */
  protected int $processed;

  /**
   * DatasetBatchContext constructor.
   *
   * @param int $chunk_size
   *   The batch chunk size.
   */
  public function __construct(int $chunk_size) {
    $this->valid = 0;
    $this->invalid = 0;
    $this->processed = 0;
    $this->finished = FALSE;
    $this->chunkSize = $chunk_size;
  }

  /**
   * A method to get the number of valid items in the batch.
   *
   * @return int
   *   The number of valid items.
   */
  public function getValid(): int {
    return $this->valid;
  }

  /**
   * A method to get the number of invalid items in the batch.
   *
   * @return int
   *   The number of invalid items.
   */
  public function getInvalid(): int {
    return $this->invalid;
  }

  /**
   * A method to get the number of items processed in the batch.
   *
   * @return int
   *   The number of items processed.
   */
  public function getProcessed(): int {
    return $this->processed;
  }

  /**
   * A method to get the last delta of the batch.
   *
   * This is simply the number of processed items minus 1 to account
   * for it starting from 0.
   *
   * @return int
   *   The last delta.
   */
  public function getLastDelta(): int {
    return $this->processed - 1;
  }

  /**
   * A method to get the batch chunk size.
   *
   * @return int
   *   The batch chunk size.
   */
  public function getChunkSize(): int {
    return $this->chunkSize;
  }

  /**
   * A method to increment the number of valid items.
   *
   * This in turn increments the number of processed items.
   *
   * @param int $count
   *   The valid count to increment.
   */
  public function incrementValid(int $count = 1): void {
    $this->valid += $count;
    $this->processed += $count;
  }

  /**
   * A method to increment the number of invalid items.
   *
   * This in turn increments the number of processed items.
   *
   * @param int $count
   *   The invalid count to increment.
   */
  public function incrementInvalid(int $count = 1): void {
    $this->invalid += $count;
    $this->processed += $count;
  }

  /**
   * A method to indicate whether the batch is finished.
   *
   * @return bool
   *   Whether the batch is finished.
   */
  public function isFinished(): bool {
    return $this->finished;
  }

  /**
   * A method to set the batch to finished.
   */
  public function setFinished(): void {
    $this->finished = TRUE;
  }

}
