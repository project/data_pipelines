<?php

namespace Drupal\Tests\data_pipelines\Exception;

/**
 * Provides an exception for when destination plugins are not found.
 */
class DestinationPluginNotFoundException extends \RuntimeException {

}
