<?php

namespace Drupal\Tests\data_pipelines\Exception;

/**
 * Provides an exception for when datasets are not found.
 */
class DatasetNotFoundException extends \RuntimeException {

}
