<?php

namespace Drupal\Tests\data_pipelines\Exception;

/**
 * Provides an exception for when destinations are not found.
 */
class DestinationNotFoundException extends \RuntimeException {

}
