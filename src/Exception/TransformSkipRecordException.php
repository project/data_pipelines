<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Exception;

/**
 * Provides an exception that is thrown when a row is skipped.
 */
class TransformSkipRecordException extends \Exception {

}
