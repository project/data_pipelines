<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Traits;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * A trait for getting a field value from an entity.
 */
trait FieldValueTrait {

  /**
   * A method to get a field value from an entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param string $field_name
   *   The field name.
   * @param string|null $main_property_name
   *   The main property name.
   *
   * @return mixed
   *   The field value.
   */
  public static function getFieldValue(ContentEntityInterface $entity, string $field_name, ?string $main_property_name = NULL) {
    /** @var \Drupal\Core\Field\BaseFieldDefinition $field_definition */
    $field_definition = $entity->{$field_name}->getFieldDefinition();
    $main_property_name = $main_property_name ?? $field_definition->getMainPropertyName();
    return $entity->{$field_name}->{$main_property_name};
  }

}
