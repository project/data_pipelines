<?php

namespace Drupal\data_pipelines\Traits;

/**
 * A trait that handles non printing characters for resources or plain strings.
 */
trait NonPrintingCharsTrait {

  /**
   * A method to remove a BOM from a resource by skipping it.
   *
   * @param resource $resource
   *   The resource.
   */
  protected function removeBom($resource): void {
    if (fgets($resource, 4) !== "\xef\xbb\xbf") {
      rewind($resource);
    }
  }

  /**
   * A method to remove DOS chars from a string.
   *
   * @param string $string
   *   The string.
   *
   * @return string
   *   The resulting string.
   */
  protected function removeDosChars(string $string): string {
    return str_replace(["\r\n", "\n", "\r", "\0", "\x0B"], '', $string);
  }

}
