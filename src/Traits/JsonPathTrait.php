<?php

namespace Drupal\data_pipelines\Traits;

use Flow\JSONPath\JSONPath;
use Flow\JSONPath\JSONPathException;

/**
 * A class with utility methods in relation to json path.
 *
 * @see \Flow\JSONPath\JSONPath
 */
trait JsonPathTrait {

  /**
   * A method to create a json path instance.
   *
   * @param array $json
   *   The json.
   *
   * @return \Flow\JSONPath\JSONPath
   *   The instance.
   */
  protected function createJsonPath(array $json): JSONPath {
    return new JSONPath($json);
  }

  /**
   * A method to validate a json path by parsing it.
   *
   * @param string $json_path
   *   The json path.
   *
   * @return bool
   *   The result.
   */
  protected function validateJsonPath(string $json_path): bool {
    try {
      $json_path_tokens = (new JSONPath())->parseTokens($json_path);
      if (empty($json_path_tokens)) {
        // This is internal for now.
        throw new JSONPathException("There are no tokens for this json path.");
      }
    }
    catch (\Exception $e) {
      return FALSE;
    }
    return TRUE;
  }

}
