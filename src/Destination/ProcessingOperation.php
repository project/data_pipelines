<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Destination;

/**
 * Defines a value object for a processing operation.
 */
final class ProcessingOperation {

  /**
   * Constructs a new ProcessingOperation.
   *
   * @param \Drupal\data_pipelines\Destination\ProcessingOperationEnum $operation
   *   Operation.
   * @param string $destinationId
   *   Destination ID.
   * @param \Drupal\data_pipelines\DatasetData[]|int[] $processingData
   *   Data.
   */
  public function __construct(private readonly ProcessingOperationEnum $operation, private readonly string $destinationId, private readonly array $processingData = []) {

  }

  /**
   * Gets value of Operation.
   *
   * @return \Drupal\data_pipelines\Destination\ProcessingOperationEnum
   *   Value of Operation.
   */
  public function getOperation(): ProcessingOperationEnum {
    return $this->operation;
  }

  /**
   * Gets value of DestinationId.
   *
   * @return string
   *   Value of DestinationId.
   */
  public function getDestinationId(): string {
    return $this->destinationId;
  }

  /**
   * Gets value of Chunk.
   *
   * @return array
   *   Value of Chunk.
   */
  public function getProcessingData(): array {
    return $this->processingData;
  }

}
