<?php

namespace Drupal\data_pipelines\Destination;

/**
 * Provides a request to save to a destination.
 */
class DestinationSaveRequest extends DestinationRequest {

}
