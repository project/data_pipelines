<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Destination;

use Drupal\data_pipelines\Entity\DatasetInterface;

/**
 * Processing operation enum.
 */
enum ProcessingOperationEnum: string {

  case Begin = 'begin';
  case ProcessChunk = 'process_chunk';
  case End = 'end';
  case ProcessCleanup = 'process_cleanup';

  /**
   * Gets the ID for this operation.
   *
   * @return string
   *   ID representation of the enum.
   */
  public function id(): string {
    return $this->value;
  }

  /**
   * Process the operation.
   *
   * @param \Drupal\data_pipelines\Destination\DatasetDestinationPluginInterface $destinationPlugin
   *   Plugin to perform processing.
   * @param \Drupal\data_pipelines\Entity\DatasetInterface $dataset
   *   Dataset.
   * @param \Drupal\data_pipelines\Destination\ProcessingOperation $operation
   *   Operation.
   *
   * @return bool
   *   Processing result.
   */
  public function process(DatasetDestinationPluginInterface $destinationPlugin, DatasetInterface $dataset, ProcessingOperation $operation): bool {
    return match($this) {
      self::Begin => $destinationPlugin->beginProcessing($dataset),
      self::End => $destinationPlugin->endProcessing($dataset),
      self::ProcessCleanup => $destinationPlugin->processCleanup($dataset, $operation->getProcessingData()),
      default => $destinationPlugin->processChunk($dataset, $operation->getProcessingData()),
    };
  }

}
