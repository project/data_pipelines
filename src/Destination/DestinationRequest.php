<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Destination;

/**
 * Defines a value object for a destination request.
 */
abstract class DestinationRequest {

  /**
   * The machine name.
   *
   * @var string
   */
  protected $machineName;

  /**
   * Label to delete.
   *
   * @var string
   */
  protected $label;

  /**
   * Constructs a new DestinationRequest.
   *
   * @param string $machine_name
   *   UUID.
   * @param string $label
   *   Label.
   */
  public function __construct(string $machine_name, string $label) {
    $this->machineName = $machine_name;
    $this->label = $label;
    @trigger_error(sprintf('%s is deprecated in data_pipelines:1.0.0-alpha19 and removed in data_pipelines:2.0.0. No replacement is provided. See https://www.drupal.org/project/data_pipelines/issues/3361740', static::class), E_USER_DEPRECATED);
  }

  /**
   * Gets value of machine name.
   *
   * @return string
   *   Value of machine name.
   */
  public function getMachineName(): string {
    return $this->machineName;
  }

  /**
   * Gets value of Label.
   *
   * @return string
   *   Value of Label.
   */
  public function getLabel(): string {
    return $this->label;
  }

}
