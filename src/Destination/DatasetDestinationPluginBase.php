<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Destination;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Plugin\PluginWithFormsTrait;
use Drupal\data_pipelines\Entity\DatasetInterface;
use Drupal\data_pipelines\Plugin\MergeableConfigurationPluginInterface;

/**
 * Defines a base class for destination plugins.
 */
abstract class DatasetDestinationPluginBase extends PluginBase implements DatasetDestinationPluginInterface, MergeableConfigurationPluginInterface {

  use PluginWithFormsTrait;

  /**
   * {@inheritdoc}
   */
  public function getLabel(): string {
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string {
    return (string) $this->pluginDefinition['description'];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration(): array {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function mergeConfiguration(array $configuration): void {
    $this->configuration = array_merge($this->configuration, $configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->setConfiguration($form_state->getValues());
  }

  /**
   * {@inheritdoc}
   */
  public function beginProcessing(DatasetInterface $dataset): bool {
    // Nil op.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function processChunk(DatasetInterface $dataset, array $chunk): bool {
    // Nil op.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function endProcessing(DatasetInterface $dataset): bool {
    // Nil op.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getLastDelta(DatasetInterface $dataset): int {
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessingChunkSize(): int {
    return self::PROCESSING_CHUNK_SIZE_ALL;
  }

  /**
   * {@inheritdoc}
   */
  public function processCleanup(DatasetInterface $dataset, array $invalidDeltas): bool {
    // Nil op.
    return TRUE;
  }

}
