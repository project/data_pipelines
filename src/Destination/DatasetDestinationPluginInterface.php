<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Destination;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Plugin\PluginWithFormsInterface;
use Drupal\data_pipelines\Entity\DatasetInterface;
use Drupal\data_pipelines\Entity\DestinationInterface;

/**
 * Provides an interface for dataset destination plugins.
 */
interface DatasetDestinationPluginInterface extends PluginFormInterface, ConfigurableInterface, PluginWithFormsInterface {

  const PROCESSING_CHUNK_SIZE_ALL = -1;

  /**
   * Gets the destination label.
   *
   * @return string
   *   The label.
   */
  public function getLabel(): string;

  /**
   * Gets the destination description.
   *
   * @return string
   *   The description.
   */
  public function getDescription(): string;

  /**
   * View the destination settings.
   *
   * @return array
   *   A render array for viewing settings.
   */
  public function viewSettings(): array;

  /**
   * Deletes a data set from the destination.
   *
   * @param \Drupal\data_pipelines\Entity\DatasetInterface $dataset
   *   The dataset.
   * @param \Drupal\data_pipelines\Entity\DestinationInterface $destination
   *   The destination.
   *
   * @return bool
   *   TRUE if the deletion succeeded.
   */
  public function deleteDataSet(DatasetInterface $dataset, DestinationInterface $destination): bool;

  /**
   * Get the last delta of the dataset from the destination.
   *
   * @param \Drupal\data_pipelines\Entity\DatasetInterface $dataset
   *   Dataset.
   *
   * @return int
   *   The last delta.
   */
  public function getLastDelta(DatasetInterface $dataset): int;

  /**
   * Gets processing chunk size.
   *
   * @return int
   *   Chunk size.
   */
  public function getProcessingChunkSize(): int;

  /**
   * Processes a chunk.
   *
   * @param \Drupal\data_pipelines\Entity\DatasetInterface $dataset
   *   Dataset.
   * @param \Drupal\data_pipelines\DatasetData[] $chunk
   *   Data to process.
   *
   * @return bool
   *   Processing result.
   */
  public function processChunk(DatasetInterface $dataset, array $chunk): bool;

  /**
   * Begin processing.
   *
   * @param \Drupal\data_pipelines\Entity\DatasetInterface $dataset
   *   Dataset.
   *
   * @return bool
   *   Begin result.
   */
  public function beginProcessing(DatasetInterface $dataset): bool;

  /**
   * End processing.
   *
   * @param \Drupal\data_pipelines\Entity\DatasetInterface $dataset
   *   Dataset.
   *
   * @return bool
   *   End result.
   */
  public function endProcessing(DatasetInterface $dataset): bool;

  /**
   * Processes cleanup of invalid records.
   *
   * @param \Drupal\data_pipelines\Entity\DatasetInterface $dataset
   *   Dataset.
   * @param int[] $invalidDeltas
   *   Deltas of invalid data.
   *
   * @return bool
   *   Cleanup result.
   */
  public function processCleanup(DatasetInterface $dataset, array $invalidDeltas): bool;

}
