<?php

namespace Drupal\data_pipelines\Destination;

/**
 * Provides a delete from destination request.
 */
class DestinationDeleteRequest extends DestinationRequest {
}
