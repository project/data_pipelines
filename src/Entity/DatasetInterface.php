<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\data_pipelines\DatasetPipelineInterface;
use Drupal\data_pipelines\Form\DatasetBatchContext;
use Drupal\data_pipelines\TransformValidDataIterator;

/**
 * Defines an interface for dataset entities.
 */
interface DatasetInterface extends ContentEntityInterface, EntityPublishedInterface {

  const STATUS_PENDING_VALIDATION = 'pending_validation';
  const STATUS_PENDING_PROCESSING = 'pending_processing';
  const STATUS_PENDING_DELETION = 'pending_deletion';
  const STATUS_PROCESSED = 'processed';

  const STATUS_INITIAL = 'initial';
  const QUEUE_NAME_PREFIX = 'data_pipelines_process:';

  const INVALID_RETAIN = 'retain';
  const INVALID_REMOVE = 'remove';

  /**
   * Gets status label.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   Status.
   */
  public function getStatusLabel(): TranslatableMarkup;

  /**
   * Sets processing as complete.
   *
   * @return $this
   */
  public function setProcessingComplete(): DatasetInterface;

  /**
   * Resets logs.
   *
   * @param bool $auto_save
   *   TRUE to autosave.
   *
   * @return $this
   */
  public function resetLogs(bool $auto_save = TRUE): DatasetInterface;

  /**
   * Adds a log entry.
   *
   * @param string $message
   *   Log message.
   * @param bool $auto_save
   *   TRUE to autosave.
   *
   * @return $this
   */
  public function addLogMessage(string $message, bool $auto_save = TRUE): DatasetInterface;

  /**
   * A method to get the data from the dataset.
   *
   * @return \Generator
   *   Generated data.
   */
  public function data(): \Generator;

  /**
   * Gets log entries.
   *
   * @return string[]
   *   Log entries.
   */
  public function getLogs(): array;

  /**
   * Set dataset status to pending validation.
   *
   * @return $this
   */
  public function setPendingValidation(): DatasetInterface;

  /**
   * Checks if status is pending validation.
   *
   * @return bool
   *   TRUE if pending validation.
   */
  public function isPendingValidation(): bool;

  /**
   * Checks if dataset is pending processing.
   *
   * @return bool
   *   TRUE if is pending processing.
   */
  public function isPendingProcessing(): bool;

  /**
   * Sets dataset status to pending processing.
   *
   * @return $this
   */
  public function setPendingProcessing(): DatasetInterface;

  /**
   * Check if the dataset is processed.
   *
   * @return bool
   *   TRUE if is processed.
   */
  public function isProcessed(): bool;

  /**
   * Sets dataset status to pending deletion.
   *
   * @return $this
   */
  public function setPendingDeletion(): DatasetInterface;

  /**
   * Checks if dataset is pending deletion.
   *
   * @return bool
   *   TRUE if is pending deletion..
   */
  public function isPendingDeletion(): bool;

  /**
   * Gets source label.
   *
   * @return string|\Drupal\Core\StringTranslation\TranslatableMarkup
   *   Source label.
   */
  public function getSourceLabel();

  /**
   * Gets the machine_name.
   *
   * @return string|null
   *   The machine_name, or NULL if the object does not yet have one.
   */
  public function getMachineName(): ?string;

  /**
   * A method to get the pipeline.
   *
   * @return \Drupal\data_pipelines\DatasetPipelineInterface
   *   The pipeline.
   */
  public function getPipeline(): DatasetPipelineInterface;

  /**
   * Gets the pipeline ID.
   *
   * @return string
   *   The pipeline id.
   */
  public function getPipelineId(): string;

  /**
   * Gets pipeline label.
   *
   * @return string|\Drupal\Core\StringTranslation\TranslatableMarkup
   *   Pipeline label.
   */
  public function getPipelineLabel();

  /**
   * Gets the dataset destinations.
   *
   * @return \Drupal\data_pipelines\Entity\DestinationInterface[]
   *   Destination plugins.
   */
  public function getDestinations(): array;

  /**
   * Loads the dataset by machine name.
   *
   * @param string $machine_name
   *   The machine_name of the entity to load.
   *
   * @return static|null
   *   The dataset or NULL if there is no dataset with the given machine_name.
   */
  public static function loadByMachineName(string $machine_name);

  /**
   * Checks if the pipeline has validation.
   *
   * @return bool
   *   TRUE if validation should occur.
   */
  public function hasValidation(): bool;

  /**
   * Queue processing for a dataset.
   *
   * @param \Drupal\Core\Queue\QueueInterface $queue
   *   Queue to use.
   * @param \Drupal\data_pipelines\Form\DatasetBatchContext $context
   *   The dataset batch context.
   */
  public function queueProcessing(QueueInterface $queue, DatasetBatchContext $context): void;

  /**
   * Gets the data iterator.
   *
   * @param int $seek_from
   *   Position to seek from. The iterator will skip all rows where the delta is
   *   less than this in order to allow fast-forwarding to a specific delta.
   *
   * @return \Drupal\data_pipelines\TransformValidDataIterator
   *   Data iterator
   */
  public function getDataIterator(int $seek_from = 0): TransformValidDataIterator;

  /**
   * Gets batch size.
   *
   * @return int
   *   Batch size
   */
  public function getBatchSize(): int;

  /**
   * Gets the processing queue ID for this dataset.
   *
   * @return string
   *   Queue ID.
   */
  public function getProcessingQueueId(): string;

  /**
   * Gets handling for invalid values.
   *
   * @return string
   *   One of the self::INVALID_ constants
   */
  public function getInvalidValuesHandling(): string;

}
