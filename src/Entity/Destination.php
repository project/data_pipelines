<?php

namespace Drupal\data_pipelines\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Plugin\DefaultSingleLazyPluginCollection;
use Drupal\data_pipelines\Destination\DatasetDestinationPluginInterface;

/**
 * Defines the Destination entity.
 *
 * @ConfigEntityType(
 *   id = "dataset_destination",
 *   label = @Translation("Data Pipeline Destination"),
 *   label_collection = @Translation("Data Pipeline Destinations"),
 *   label_singular = @Translation("destination"),
 *   label_plural = @Translation("destinations"),
 *   label_count = @PluralTranslation(
 *     singular = "@count destination",
 *     plural = "@count destinations",
 *   ),
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   handlers = {
 *     "list_builder" = \Drupal\data_pipelines\EntityHandlers\DatasetDestinationListBuilder::class,
 *     "route_provider" = {
 *       "html" = "\Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "form" = {
 *       "default" = "Drupal\data_pipelines\Form\DestinationForm",
 *       "add" = "Drupal\data_pipelines\Form\DestinationForm",
 *       "edit" = "Drupal\data_pipelines\Form\DestinationForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   admin_permission = "administer data_pipelines",
 *   links = {
 *     "add-form" = "/admin/config/content/dataset_destinations/add",
 *     "edit-form" = "/admin/config/content/dataset_destinations/manage/{dataset_destination}",
 *     "delete-form" = "/admin/config/content/dataset_destinations/manage/{dataset_destination}/delete",
 *     "collection" = "/admin/config/content/dataset_destinations"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "destination",
 *     "destinationSettings",
 *   }
 * )
 */
class Destination extends ConfigEntityBase implements DestinationInterface {

  /**
   * The destination plugin ID.
   *
   * @var string
   */
  protected $destination;

  /**
   * The destination plugin collection.
   *
   * @var \Drupal\Core\Plugin\DefaultSingleLazyPluginCollection
   */
  protected $destinationCollection;

  /**
   * The plugin configuration.
   *
   * @var array
   */
  protected $destinationSettings = [];

  /**
   * {@inheritdoc}
   */
  public function getDestinationPluginId(): ?string {
    return $this->destination;
  }

  /**
   * {@inheritdoc}
   */
  public function getDestinationSettings(): array {
    return $this->destinationSettings;
  }

  /**
   * {@inheritdoc}
   */
  public function getDestinationPlugin(): ?DatasetDestinationPluginInterface {
    $this->preparePluginCollection();
    if (is_null($this->destinationCollection)) {
      return NULL;
    }
    return $this->destinationCollection->get($this->destination);
  }

  /**
   * Prepare the plugin collection.
   */
  protected function preparePluginCollection(): void {
    if (is_null($this->destinationCollection) && !is_null($this->destination) && $this->destination !== '') {
      $this->destinationCollection = new DefaultSingleLazyPluginCollection(\Drupal::service('plugin.manager.data_pipelines_destination'), $this->destination, $this->destinationSettings);
    }
  }

}
