<?php

namespace Drupal\data_pipelines\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\data_pipelines\Destination\DatasetDestinationPluginInterface;

/**
 * Provides an interface for the destination config entity.
 */
interface DestinationInterface extends ConfigEntityInterface {

  /**
   * Gets the plugin ID.
   *
   * @return string|null
   *   The plugin ID.
   */
  public function getDestinationPluginId(): ?string;

  /**
   * Returns the operation plugin.
   *
   * @return \Drupal\data_pipelines\Destination\DatasetDestinationPluginInterface|null
   *   The plugin.
   */
  public function getDestinationPlugin(): ?DatasetDestinationPluginInterface;

  /**
   * Gets the destination settings.
   *
   * @return array
   *   The destination settings.
   */
  public function getDestinationSettings(): array;

}
