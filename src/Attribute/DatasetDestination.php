<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines a DatasetDestination attribute for plugin discovery.
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class DatasetDestination extends Plugin {

  /**
   * Constructs a DatasetDestination attribute.
   *
   * @param string $id
   *   The plugin ID.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $label
   *   The plugin label.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|null $description
   *   (optional) A description for the destination plugin.
   */
  public function __construct(
    public readonly string $id,
    public readonly TranslatableMarkup $label,
    public readonly ?TranslatableMarkup $description = NULL,
  ) {}

}
