<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;

/**
 * Defines a DatasetTransform attribute for plugin discovery.
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class DatasetTransform extends Plugin {

  /**
   * Constructs a DatasetTransform attribute.
   *
   * @param string $id
   *   The plugin ID.
   * @param bool $fields
   *   (optional) Whether the transform plugin supports fields.
   * @param bool $records
   *   (optional) Whether the transform plugin supports records.
   */
  public function __construct(
    public readonly string $id,
    public readonly bool $fields = FALSE,
    public readonly bool $records = FALSE,
  ) {}

}
