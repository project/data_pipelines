<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Transform;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\data_pipelines\DatasetData;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a base class for transform plugins.
 */
abstract class TransformPluginBase extends PluginBase implements TransformInterface, ConfigurableInterface, ContainerFactoryPluginInterface {

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public function setConfiguration(array $configuration): void {
    $this->configuration = $configuration;
  }

  /**
   * Constructs a new TransformPluginBase.
   *
   * @param array $configuration
   *   Configuration.
   * @param string $plugin_id
   *   Plugin ID.
   * @param array $plugin_definition
   *   Definition.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   Logger.
   */
  final public function __construct(array $configuration, string $plugin_id, array $plugin_definition, LoggerChannelInterface $logger) {
    $configuration = $configuration + $this->defaultConfiguration();
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.channel.data_pipelines')
    );
  }

  /**
   * Checks if the transform plugin supports fields.
   *
   * @return bool
   *   TRUE if supports transforming a field.
   */
  public function supportsFields(): bool {
    return $this->pluginDefinition['fields'] === TRUE;
  }

  /**
   * Checks if the transform plugin supports records.
   *
   * @return bool
   *   TRUE if supports transforming a record.
   */
  public function supportsRecords(): bool {
    return $this->pluginDefinition['records'] === TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function transformField(string $field_name, DatasetData $record): DatasetData {
    if (!$this->supportsFields()) {
      $this->logger->warning(sprintf('Using transform %s to transform fields is not support.', $this->pluginId));
      return $record;
    }
    return $this->doTransformField($field_name, $record);
  }

  /**
   * {@inheritdoc}
   */
  public function transformRecord(DatasetData $record): DatasetData {
    if (!$this->supportsRecords()) {
      $this->logger->warning(sprintf('Using transform %s to transform records is not support.', $this->pluginId));
      return $record;
    }
    return $this->doTransformRecord($record);
  }

  /**
   * Transform field value.
   *
   * @param string $field_name
   *   Field name to transform.
   * @param \Drupal\data_pipelines\DatasetData $record
   *   The record containing the field to transform.
   *
   * @return \Drupal\data_pipelines\DatasetData
   *   The record with a transformed value.
   */
  protected function doTransformField(string $field_name, DatasetData $record): DatasetData {
    return $record;
  }

  /**
   * Transform record.
   *
   * @param \Drupal\data_pipelines\DatasetData $record
   *   The whole row.
   *
   * @return \Drupal\data_pipelines\DatasetData
   *   The transformed record.
   */
  protected function doTransformRecord(DatasetData $record): DatasetData {
    return $record;
  }

}
