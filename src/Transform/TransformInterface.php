<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Transform;

use Drupal\data_pipelines\DatasetData;

/**
 * Defines an interface for transform plugins.
 */
interface TransformInterface {

  /**
   * Transform field value.
   *
   * @param string $field_name
   *   Field name to transform.
   * @param \Drupal\data_pipelines\DatasetData $record
   *   The record containing the field to transform.
   *
   * @return \Drupal\data_pipelines\DatasetData
   *   The record with a transformed value.
   */
  public function transformField(string $field_name, DatasetData $record): DatasetData;

  /**
   * Transform record.
   *
   * @param \Drupal\data_pipelines\DatasetData $record
   *   The whole row.
   *
   * @return \Drupal\data_pipelines\DatasetData
   *   The transformed record.
   */
  public function transformRecord(DatasetData $record): DatasetData;

}
