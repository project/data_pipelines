<?php

namespace Drupal\data_pipelines\Commands;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\data_pipelines\Entity\Dataset;
use Drupal\data_pipelines\Entity\DatasetInterface;
use Drupal\data_pipelines\Form\DatasetBatchOperations;
use Drush\Commands\DrushCommands;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Data pipelines drush commands.
 */
class DataPipelinesCommands extends DrushCommands {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Creates a DataPipelinesCommand object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Connection $database) {
    parent::__construct();
    $this->entityTypeManager = $entity_type_manager;
    $this->database = $database;
  }

  /**
   * Provide a list of datasets if a machine name has not been provided.
   *
   * @hook interact data-pipelines:reindex
   */
  public function datasetReindexInteract(InputInterface $input, OutputInterface $output): void {
    if (!empty($input->getArgument('machine_name'))) {
      return;
    }
    $datasets = $this->entityTypeManager->getStorage('data_pipelines')->loadMultiple();
    $choices = ['all' => 'All'];
    foreach ($datasets as $dataset) {
      assert($dataset instanceof DatasetInterface);
      $choices[$dataset->getMachineName()] = $dataset->label();
    }
    $choice = $this->io()->choice(dt('Which dataset would you like to reindex'), $choices);
    $input->setArgument('machine_name', $choice);
  }

  /**
   * Delete and reindex dataset(s).
   *
   * @param string $machine_name
   *   The dataset machine name.
   *
   * @command data-pipelines:reindex
   *
   * @validate-module-enabled data_pipelines
   *
   * @throws \Exception
   */
  public function datasetReindex(string $machine_name): void {
    $datasets = [];
    if ('all' === $machine_name) {
      $results = $this->database->select('data_pipelines')
        ->fields('data_pipelines', ['machine_name'])
        ->execute()
        ->fetchAll();
      $datasets = array_map(fn($result) => $result->machine_name, $results);
    }
    else {
      $datasets[] = $machine_name;
    }
    foreach ($datasets as $dataset) {
      $dataset = Dataset::loadByMachineName($dataset);
      assert($dataset instanceof DatasetInterface);
      foreach ($dataset->getDestinations() as $destination) {
        $destination->getDestinationPlugin()->deleteDataSet($dataset, $destination);
      }
      batch_set(DatasetBatchOperations::batchForDataset($dataset));
    }
    drush_backend_batch_process();
  }

}
