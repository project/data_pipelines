<?php

declare(strict_types=1);

namespace Drupal\data_pipelines;

use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * Interface DatasetInterface.
 *
 * @package Drupal\data_pipelines
 */
interface DatasetPipelineInterface {

  /**
   * A method to validate the data from the dataset source.
   *
   * @param \Drupal\data_pipelines\DatasetData $data
   *   The dataset data.
   *
   * @return \Symfony\Component\Validator\ConstraintViolationListInterface
   *   Violations.
   */
  public function validate(DatasetData $data): ConstraintViolationListInterface;

  /**
   * A method to transform the dataset data.
   *
   * @param \Drupal\data_pipelines\DatasetData $data
   *   The dataset data.
   *
   * @throws \Drupal\data_pipelines\Exception\TransformSkipRecordException
   */
  public function transform(DatasetData $data): DatasetData;

  /**
   * Checks if the pipeline has validation.
   *
   * @return bool
   *   TRUE if validation should occur.
   */
  public function hasValidation(): bool;

  /**
   * A method to get the destination settings defined as part of the pipeline.
   *
   * @param string $destination_id
   *   The destination ID.
   *
   * @return array
   *   The destination settings.
   */
  public function getDestinationSettings(string $destination_id): array;

}
