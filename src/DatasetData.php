<?php

declare(strict_types=1);

namespace Drupal\data_pipelines;

/**
 * Defines a dataset data object.
 */
final class DatasetData extends \ArrayObject {
}
