<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Source;

use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\data_pipelines\Entity\DatasetInterface;
use Drupal\data_pipelines\Source\Resource\SourceResourceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A base class for sources.
 */
abstract class DatasetSourceBase extends PluginBase implements DatasetSourceInterface, ContainerFactoryPluginInterface {

  /**
   * The source resource.
   *
   * @var \Drupal\data_pipelines\Source\Resource\SourceResourceBase
   */
  protected $sourceResource;

  /**
   * The logger channel service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerChannel;

  /**
   * DatasetSourceBase constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin Id.
   * @param array $plugin_definition
   *   The plugin definition.
   * @param \Drupal\data_pipelines\Source\Resource\SourceResourceInterface $source_resource
   *   The source resource.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger_channel
   *   The logger channel service.
   */
  final public function __construct(array $configuration, string $plugin_id, array $plugin_definition, SourceResourceInterface $source_resource, LoggerChannelInterface $logger_channel) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->sourceResource = $source_resource;
    $this->loggerChannel = $logger_channel;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get($plugin_definition['source_resource_service']),
      $container->get('logger.channel.data_pipelines')
    );
  }

  /**
   * Gets the resource for a dataset.
   *
   * @param \Drupal\data_pipelines\Entity\DatasetInterface $dataset
   *   Dataset.
   *
   * @return resource|null
   *   Resource or null if not able to create a resource.
   *
   * @throws \Exception
   */
  protected function getResource(DatasetInterface $dataset) {
    $resource = $this->sourceResource->getResource($dataset, $this->getResourceFieldName());
    if (!is_resource($resource)) {
      throw new \Exception("Unable to get the resource.");
    }
    return $resource;
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions(): array {
    $fields = [$this->getResourceFieldName() => $this->sourceResource->getResourceBaseFieldDefinition($this->pluginDefinition)];
    return $fields;
  }

  /**
   * Gets field name for storage of the resource.
   *
   * @return string
   *   The resource field name.
   */
  protected function getResourceFieldName(): string {
    return sprintf('%s_%s', $this->getBaseId(), $this->pluginDefinition['source_resource_id']);
  }

  /**
   * A method to extract the data from the dataset.
   *
   * @param \Drupal\data_pipelines\Entity\DatasetInterface $dataset
   *   The dataset.
   *
   * @return \Generator
   *   The data.
   */
  abstract public function extractDataFromDataSet(DatasetInterface $dataset): \Generator;

}
