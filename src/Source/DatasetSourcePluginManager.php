<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Source;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\data_pipelines\Attribute\DatasetSource;
use Drupal\data_pipelines\Annotation\DatasetSource as DatasetSourceAnnotation;

/**
 * Defines a plugin manager for source plugins.
 */
class DatasetSourcePluginManager extends DefaultPluginManager {

  /**
   * Constructs the DatasetSourcePluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/DatasetSource', $namespaces, $module_handler, DatasetSourceInterface::class, DatasetSource::class, DatasetSourceAnnotation::class);
    $this->alterInfo('data_pipelines_source');
    $this->setCacheBackend($cache_backend, 'data_pipelines_source');
  }

}
