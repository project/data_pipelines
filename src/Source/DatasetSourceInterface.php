<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Source;

use Drupal\entity\BundlePlugin\BundlePluginInterface;

/**
 * Defines an interface for dataset sources.
 */
interface DatasetSourceInterface extends BundlePluginInterface {

}
