<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Source\Resource;

/**
 * Defines a class to manage source resources.
 */
class SourceResourceManager {

  /**
   * The source resources.
   *
   * @var array
   */
  protected $sourceResources = [];

  /**
   * SourceResourceManager constructor.
   *
   * @param array $source_resources
   *   The source resources.
   *
   * @throws \Exception
   */
  public function __construct(array $source_resources) {
    foreach ($source_resources as $source_resource) {
      $source_resource_service_name_split = explode('.', $source_resource);
      if (empty($source_resource_service_name_split) || 1 === count($source_resource_service_name_split)) {
        throw new \InvalidArgumentException("The source resource service name must be delimited by a period (.)");
      }
      $source_resource_id = end($source_resource_service_name_split);
      if (preg_match('/[^\w]+/', $source_resource_id)) {
        throw new \InvalidArgumentException("The source resource id must be alphanumeric.");
      }
      $this->sourceResources[$source_resource_id] = $source_resource;
    }
  }

  /**
   * A method to get the source resources.
   *
   * @return array
   *   The source resources.
   */
  public function getSourceResources(): array {
    return $this->sourceResources;
  }

}
