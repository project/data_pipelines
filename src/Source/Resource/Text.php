<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Source\Resource;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\data_pipelines\Entity\DatasetInterface;

/**
 * A class for the string resource.
 */
final class Text extends SourceResourceBase {

  /**
   * {@inheritdoc}
   */
  public function getResource(DatasetInterface $dataset, string $field_name): mixed {
    if (!empty($string = self::getFieldValue($dataset, $field_name))) {
      if ($resource = fopen('php://temp', 'r+')) {
        fwrite($resource, $string);
        rewind($resource);
        return $resource;
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getResourceBaseFieldDefinition(array $source_plugin_definition): BaseFieldDefinition {
    return BaseFieldDefinition::create('string_long')
      ->setLabel(new TranslatableMarkup('Data'))
      ->setDescription(new TranslatableMarkup('The data for the dataset'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => 25,
        'settings' => [
          'rows' => 4,
        ],
      ]);
  }

}
