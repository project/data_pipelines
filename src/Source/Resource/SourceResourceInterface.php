<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Source\Resource;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\data_pipelines\Entity\DatasetInterface;

/**
 * Defines an interface for source resources.
 */
interface SourceResourceInterface {

  /**
   * Gets a resource from a dataset.
   *
   * @param \Drupal\data_pipelines\Entity\DatasetInterface $dataset
   *   Dataset.
   * @param string $field_name
   *   Field name.
   *
   * @return resource
   *   A resource.
   */
  public function getResource(DatasetInterface $dataset, string $field_name): mixed;

  /**
   * Gets the base field definition for the resource field.
   *
   * @param array $source_plugin_definition
   *   Plugin definition.
   *
   * @return \Drupal\Core\Field\BaseFieldDefinition
   *   Base field definition.
   */
  public function getResourceBaseFieldDefinition(array $source_plugin_definition): BaseFieldDefinition;

}
