<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Source\Resource;

use Drupal\data_pipelines\Traits\FieldValueTrait;

/**
 * A base class for source resources.
 */
abstract class SourceResourceBase implements SourceResourceInterface {

  use FieldValueTrait;

}
