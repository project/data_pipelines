<?php

namespace Drupal\data_pipelines\EntityHandlers;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Defines a route provider for dataset content entities.
 */
class DatasetRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);
    $entity_type_id = $entity_type->id();
    $collection->add("entity.{$entity_type_id}.process_form",
      (new Route($entity_type->getLinkTemplate('process-form')))
        ->addDefaults([
          '_entity_form' => "{$entity_type_id}.process",
          '_title_callback' => '\Drupal\Core\Entity\Controller\EntityController::editTitle',
        ])
        ->setRequirement('_entity_access', "{$entity_type_id}.update")
        ->setOption('_admin_route', TRUE)
        ->setOption('parameters', [
          $entity_type_id => ['type' => 'entity:' . $entity_type_id],
        ])
        ->setRequirement($entity_type_id, '\d+'));
    return $collection;
  }

}
