<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\EntityHandlers;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Defines a storage handler for Dataset entities.
 */
class DatasetStorage extends SqlContentEntityStorage {

  /**
   * {@inheritdoc}
   */
  public function delete(array $entities) {
    /** @var \Drupal\data_pipelines\Entity\DatasetInterface $dataset */
    foreach ($entities as $dataset) {
      foreach ($dataset->getDestinations() as $destination) {
        $destination->getDestinationPlugin()->deleteDataSet($dataset, $destination);
      }
    }
    parent::delete($entities);
  }

}
