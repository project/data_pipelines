<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\EntityHandlers;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\data_pipelines\Entity\DatasetInterface;

/**
 * Provides a list builder for datasets.
 *
 * @codeCoverageIgnore
 * @see \Drupal\Tests\data_pipelines\Functional\DatasetUiTest
 */
class DatasetListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    return [
      'name' => $this->t('Label'),
      'plugin' => $this->t('Pipeline'),
      'source' => $this->t('Source'),
      'destinations' => $this->t('Destinations'),
      'status' => $this->t('Status'),
    ] + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);
    if ($entity->access('update') && $entity->hasLinkTemplate('process-form')) {
      $operations['process'] = [
        'title' => $this->t('Process'),
        'weight' => 15,
        'url' => $this->ensureDestination($entity->toUrl('process-form')),
      ];
    }
    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    assert($entity instanceof DatasetInterface);
    $destinationLabels = [];
    $destinations = $entity->getDestinations();
    foreach ($destinations as $destination) {
      $destinationLabels[] = $destination->label();
    }
    return [
      'name' => $entity->toLink(),
      'plugin' => $entity->getPipelineLabel(),
      'source' => $entity->getSourceLabel(),
      'destinations' => implode(', ', $destinationLabels),
      'status' => $entity->getStatusLabel(),
    ] + parent::buildRow($entity);
  }

}
