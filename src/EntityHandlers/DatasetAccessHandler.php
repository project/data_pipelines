<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\EntityHandlers;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultNeutral;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines an access handler for the dataset content entity.
 */
class DatasetAccessHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    $result = parent::checkAccess($entity, $operation, $account);
    if (!$result instanceof AccessResultNeutral) {
      return $result;
    }

    if ($operation === 'delete') {
      return AccessResult::allowedIfHasPermission($account, 'data_pipelines delete');
    }
    if ($operation === 'update') {
      return AccessResult::allowedIfHasPermission($account, 'data_pipelines edit');
    }
    if ($operation === 'view' || $operation === 'view label') {
      return AccessResult::allowedIfHasPermission($account, 'data_pipelines list');
    }
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    $result = parent::checkCreateAccess($account, $context, $entity_bundle);
    if (!$result instanceof AccessResultNeutral) {
      return $result;
    }

    return AccessResult::allowedIfHasPermission($account, 'data_pipelines create');
  }

}
