<?php

namespace Drupal\data_pipelines\EntityHandlers;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a list builder for dataset destination config entities.
 */
class DatasetDestinationListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function load() {
    $entity_ids = $this->getEntityIds();
    // We want overrides.
    $entities = $this->storage->loadMultiple($entity_ids);

    // Sort the entities using the entity class's sort() method.
    // See \Drupal\Core\Config\Entity\ConfigEntityBase::sort().
    uasort($entities, [$this->entityType->getClass(), 'sort']);
    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['label'] = $this->t('Label');
    $header['type'] = $this->t('Type');
    $header['details'] = $this->t("Details");
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\data_pipelines\Entity\DestinationInterface $destination */
    $destination = $entity;
    $row['label'] = $destination->label();
    $row['type'] = $destination->getDestinationPluginId();
    $row['details'] = $destination->getDestinationPlugin()->viewSettings();

    return $row + parent::buildRow($entity);
  }

}
