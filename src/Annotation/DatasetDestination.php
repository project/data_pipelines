<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * An annotation for dataset destination plugins.
 *
 * @Annotation
 */
class DatasetDestination extends Plugin {

  /**
   * The destination Id.
   *
   * @var string
   */
  public $id;

  /**
   * The destination label.
   *
   * @var string
   */
  public $label;

  /**
   * The destination description.
   *
   * @var string
   */
  public $description;

}
