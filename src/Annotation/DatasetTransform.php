<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * An annotation for dataset transform plugins.
 *
 * @Annotation
 */
class DatasetTransform extends Plugin {

  /**
   * The transform Id.
   *
   * @var string
   */
  public $id;

  /**
   * TRUE if supports fields.
   *
   * @var bool
   */
  public $fields = TRUE;

  /**
   * TRUE if supports records.
   *
   * @var bool
   */
  public $records = TRUE;

}
