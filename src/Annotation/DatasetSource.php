<?php

declare(strict_types=1);

namespace Drupal\data_pipelines\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * An annotation for dataset source plugins.
 *
 * @Annotation
 */
class DatasetSource extends Plugin {

  /**
   * The source Id.
   *
   * @var string
   */
  public $id;

  /**
   * The source label.
   *
   * @var string
   */
  public $label;

  /**
   * The source description.
   *
   * @var string
   */
  public $description;

}
