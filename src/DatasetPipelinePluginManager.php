<?php

declare(strict_types=1);

namespace Drupal\data_pipelines;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Plugin\Discovery\ContainerDerivativeDiscoveryDecorator;
use Drupal\Core\Plugin\Discovery\YamlDiscovery;

/**
 * Defines a plugin manager for pipeline plugins.
 */
class DatasetPipelinePluginManager extends DefaultPluginManager {

  /**
   * Constructs a DatasetPipelinePluginManager object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   */
  public function __construct(ModuleHandlerInterface $module_handler, CacheBackendInterface $cache_backend) {
    $this->alterInfo('data_pipelines_info');
    $this->moduleHandler = $module_handler;
    $this->setCacheBackend($cache_backend, 'data_pipelines_info', ['data_pipelines_info']);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDiscovery() {
    if (!isset($this->discovery)) {
      $this->discovery = new YamlDiscovery('data_pipelines', $this->moduleHandler->getModuleDirectories());
      $this->discovery->addTranslatableProperty('label');
      $this->discovery = new ContainerDerivativeDiscoveryDecorator($this->discovery);
    }
    return $this->discovery;
  }

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id) {
    parent::processDefinition($definition, $plugin_id);
    $definition += [
      'validations' => [],
      'transforms' => [],
      'destinationSettings' => [],
    ];
    $definition['validations'] += [
      'record' => [],
      'field' => [],
    ];
    $definition['transforms'] += [
      'record' => [],
      'field' => [],
    ];
    if (isset($definition['class']) && !in_array(DatasetPipelineInterface::class, class_implements($definition['class']), TRUE)) {
      throw new InvalidPluginDefinitionException(sprintf('Dataset pipeline plugin %s defines a class that does not implement %s', $plugin_id, DatasetPipelineInterface::class));
    }
    $definition += [
      'class' => DatasetPipelineBase::class,
    ];
  }

}
