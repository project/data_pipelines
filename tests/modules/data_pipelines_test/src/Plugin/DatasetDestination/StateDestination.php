<?php

namespace Drupal\data_pipelines_test\Plugin\DatasetDestination;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\data_pipelines\Destination\DatasetDestinationPluginBase;
use Drupal\data_pipelines\Entity\DatasetInterface;
use Drupal\data_pipelines\Entity\DestinationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A class for providing JSON as an output.
 *
 * @DatasetDestination(
 *   id="state",
 *   label="State",
 *   description="A state destination used for testing"
 * )
 */
class StateDestination extends DatasetDestinationPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a new FileDestinationBase.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  final public function __construct(array $configuration, $plugin_id, $plugin_definition, StateInterface $state) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('state')
    );
  }

  /**
   * Gets the state key.
   *
   * @return string
   *   The state key.
   */
  public function getStateKey(): string {
    return $this->configuration['state_key'] ?? 'data_pipelines_test';
  }

  /**
   * Gets the state data.
   *
   * @return array
   *   The data.
   */
  public function getData(): array {
    return $this->state->get($this->getStateKey()) ?? [];
  }

  /**
   * Sets the state data.
   *
   * @param array $data
   *   The data.
   */
  public function setData(array $data): void {
    $this->state->set($this->getStateKey(), $data);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteDataSet(DatasetInterface $dataset, DestinationInterface $destination): bool {
    $data = $this->getData();
    unset($data[$dataset->id()]);
    $this->setData($data);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function viewSettings(): array {
    return [
      'data' => [
        '#markup' => new TranslatableMarkup("Only for testing"),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getLastDelta(DatasetInterface $dataset): int {
    $data = $this->getData();
    return isset($data[$dataset->id()]) ? count($data[$dataset->id()]) : 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessingChunkSize(): int {
    return 1;
  }

  /**
   * {@inheritdoc}
   */
  public function beginProcessing(DatasetInterface $dataset): bool {
    parent::beginProcessing($dataset);
    $this->setData([$dataset->id() => []]);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function processChunk(DatasetInterface $dataset, array $chunk): bool {
    parent::processChunk($dataset, $chunk);
    $existing = $this->getData();
    $existing[$dataset->id()] = $existing[$dataset->id()] + $chunk;
    $this->setData($existing);
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function processCleanup(DatasetInterface $dataset, array $invalidDeltas): bool {
    parent::processCleanup($dataset, $invalidDeltas);
    $existing = $this->getData();
    $existing[$dataset->id()] = array_diff_key($existing[$dataset->id()], array_flip($invalidDeltas));
    $this->setData($existing);
    return TRUE;
  }

}
