<?php

declare(strict_types=1);

namespace Drupal\Tests\data_pipelines\Unit\Plugin\DatasetDestination;

use Drupal\Tests\data_pipelines\Kernel\DatasetKernelTestBase;
use Drupal\data_pipelines\Entity\Dataset;
use Drupal\data_pipelines\Entity\Destination;
use Drupal\data_pipelines\Plugin\DatasetDestination\NullDestination;

/**
 * A class to test the Null destination plugin.
 *
 * @group data_pipelines
 * @coversDefaultClass \Drupal\data_pipelines\Plugin\DatasetDestination\NullDestination
 */
class NullDestinationTest extends DatasetKernelTestBase {

  /**
   * @covers ::saveDataSet
   * @covers ::deleteDataSet
   */
  public function testSaveDataDeleteData() {
    $destination_plugin = new NullDestination([], 'null', []);

    $dataset = Dataset::create(['source' => 'csv:text']);
    $destination = Destination::create();

    $this->assertTrue($destination_plugin->saveDataSet($dataset, $destination));
    $this->assertTrue($destination_plugin->deleteDataSet($dataset, $destination));
  }

}
