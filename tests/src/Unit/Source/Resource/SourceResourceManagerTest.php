<?php

declare(strict_types=1);

namespace Drupal\Tests\data_pipelines\Unit\Source\Resource;

use Drupal\data_pipelines\Source\Resource\SourceResourceManager;
use PHPUnit\Framework\TestCase;

/**
 * A class for testing the source resource manager.
 *
 * @group data_pipelines
 *
 * @covers \Drupal\data_pipelines\Source\Resource\SourceResourceManager
 */
class SourceResourceManagerTest extends TestCase {

  /**
   * A test to ensure the service name is delimited by periods (.).
   */
  public function testNonDelimitedServiceName() {
    $source_resource_services = ['blah', 'blah1'];

    $this->expectException(\Exception::class);
    $this->expectExceptionMessage('The source resource service name must be delimited by a period (.)');
    new SourceResourceManager($source_resource_services);
  }

  /**
   * A test to ensure the service name is alphanumeric.
   */
  public function testNonAlphanumericServiceName() {
    $source_resource_services = ['blah.blah%', 'blah1.%blah1'];

    $this->expectException(\Exception::class);
    $this->expectExceptionMessage('The source resource id must be alphanumeric.');
    new SourceResourceManager($source_resource_services);
  }

}
