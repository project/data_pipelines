<?php

declare(strict_types=1);

namespace Drupal\Tests\data_pipelines\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\data_pipelines\DatasetData;
use Drupal\data_pipelines\Entity\Dataset;
use Drupal\data_pipelines\Entity\DatasetInterface;
use Drupal\data_pipelines\Entity\Destination;

/**
 * Defines a test for UI aspects of the module.
 *
 * @group data_pipelines
 *
 * @covers \Drupal\data_pipelines\EntityHandlers\DatasetRouteProvider
 * @covers \Drupal\data_pipelines\EntityHandlers\DatasetListBuilder
 * @covers \Drupal\data_pipelines\Form\DatasetForm
 * @covers \Drupal\data_pipelines\Form\DatasetProcessForm
 */
class DatasetUiTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'options',
    'link',
    'block',
    'file',
    'entity',
    'data_pipelines',
    'data_pipelines_test',
    'user',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->drupalPlaceBlock('system_messages_block');
    $this->drupalPlaceBlock('local_actions_block');
  }

  /**
   * Tests UI aspects of the module.
   */
  public function testUi(): void {
    $this->assertAnonymousUserCannotAccessListing();

    $destination_id = 'test_destination';
    $destination = Destination::create([
      'id' => $destination_id,
      'label' => 'Test destination',
      'destination' => 'file_json',
      'destinationSettings' => [
        'scheme' => 'public',
        'dir' => '',
      ],
    ]);
    $destination->save();

    $dataset = Dataset::create([
      'pipeline' => 'test_pipeline_1',
      'source' => 'csv:file',
      'destinations' => [$destination],
      'name' => $this->randomMachineName(),
      'machine_name' => mb_strtolower($this->randomMachineName()),
      'status' => DatasetInterface::STATUS_PENDING_VALIDATION,
    ]);
    $dataset->save();
    $this->assertAnonymousUserCannotAccessDataset($dataset);
    $this->drupalLogin($this->createUser([
      'data_pipelines create',
      'data_pipelines edit',
      'data_pipelines list',
      'data_pipelines delete',
      'access content',
    ]));
    $this->assertAdminCanViewListing($dataset);
    $this->assertAdminCanEditDataset($dataset);
    $dataset = $this->assertAdminCanAddDataset();
    $this->assertAdminCanReprocessDataset($dataset);
    $this->assertAdminCanViewDatasetLogs($dataset);
  }

  /**
   * Assert anonymous user can't access listing.
   */
  protected function assertAnonymousUserCannotAccessListing(): void {
    $this->drupalGet(Url::fromRoute('entity.data_pipelines.collection'));
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Assert anonymous user can't access datasets.
   */
  protected function assertAnonymousUserCannotAccessDataset(DatasetInterface $dataset): void {
    $this->drupalGet($dataset->toUrl());
    $assert = $this->assertSession();
    $assert->statusCodeEquals(403);
    $this->drupalGet($dataset->toUrl('edit-form'));
    $assert->statusCodeEquals(403);
    $this->drupalGet($dataset->toUrl('delete-form'));
    $assert->statusCodeEquals(403);
    $this->drupalGet($dataset->toUrl('process-form'));
    $assert->statusCodeEquals(403);
  }

  /**
   * Asserts admin can view listing.
   */
  protected function assertAdminCanViewListing(DatasetInterface $dataset): void {
    $this->drupalGet(Url::fromRoute('entity.data_pipelines.collection'));
    $assert = $this->assertSession();
    $assert->statusCodeEquals(200);
    $assert->pageTextContains($dataset->label());
    $assert->pageTextContains('Test pipeline 1');
    $assert->pageTextContains('CSV File');
    $assert->pageTextContains('Pending validation');
    $row = $assert->elementExists('css', sprintf('tr:contains("%s")', $dataset->label()));
    $assert->elementExists('named', ['link', 'Edit'], $row);
    $assert->elementExists('named', ['link', 'Delete'], $row);
    $assert->elementExists('named', ['link', 'Process'], $row);
    $assert->linkExists('Add dataset');
  }

  /**
   * Asserts admin can edit dataset.
   *
   * @param \Drupal\data_pipelines\Entity\DatasetInterface $dataset
   *   Dataset.
   */
  protected function assertAdminCanEditDataset(DatasetInterface $dataset): void {
    $this->drupalGet($dataset->toUrl('edit-form'));
    $new_label = $this->randomMachineName();
    $this->assertSession()->fieldDisabled('machine_name');
    $this->submitForm([
      'files[csv_file_0]' => \Drupal::service('file_system')->realpath(dirname(__DIR__) . '/fixtures/test-pipeline-1.csv'),
      'name[0][value]' => $new_label,
      'destinations' => 'test_destination',
    ], 'Save');
    $dataset = \Drupal::entityTypeManager()->getStorage('data_pipelines')->loadUnchanged($dataset->id());
    assert($dataset instanceof DatasetInterface);
    $this->assertEquals($new_label, $dataset->label());
    $data = iterator_to_array($dataset->getDataIterator());
    $this->assertCount(2, $data);
    $this->assertEquals([
      new DatasetData([
        'should_we' => TRUE,
        'full_name' => 'bloggs, joe',
      ]),
      new DatasetData([
        'should_we' => FALSE,
        'full_name' => 'bloggs, betty',
      ]),
    ], $data);
    $this->assertEquals('Pending validation', $dataset->getStatusLabel());
    $assert = $this->assertSession();
    $assert->pageTextContains(sprintf('Saved dataset %s.', $new_label));
    $this->assertStringContainsString(Url::fromRoute('entity.data_pipelines.collection')->toString(), $this->getSession()->getCurrentUrl());
    $this->assertEquals('Pending validation', $dataset->getStatusLabel());
  }

  /**
   * Asserts admin can add dataset.
   *
   * @return \Drupal\data_pipelines\Entity\DatasetInterface
   *   Created dataset.
   */
  protected function assertAdminCanAddDataset(): DatasetInterface {
    $this->drupalGet(Url::fromRoute('entity.data_pipelines.collection'));
    $this->clickLink('Add dataset');
    $this->assertStringContainsString(Url::fromRoute('entity.data_pipelines.add_page')->toString(), $this->getSession()->getCurrentUrl());
    $assert = $this->assertSession();
    foreach (\Drupal::service('plugin.manager.data_pipelines_source')->getDefinitions() as $definition) {
      $assert->linkExists($definition['label']);
    }
    $this->clickLink('CSV Text');
    $label = $this->randomMachineName();
    $csv = <<<CSV
should_we,firstname,lastname
Y,joe,bloggs
N,betty,bloggs
CSV;
    $this->submitForm([
      'csv_text[0][value]' => $csv,
      'name[0][value]' => $label,
      'destinations' => 'test_destination',
      'machine_name' => mb_strtolower($label),
      'pipeline' => 'test_pipeline_1',
    ], 'Save');
    $assert->pageTextContains(sprintf('Created dataset %s.', $label));
    $this->assertStringContainsString(Url::fromRoute('entity.data_pipelines.collection')->toString(), $this->getSession()->getCurrentUrl());
    $dataset = Dataset::loadByMachineName(mb_strtolower($label));
    $this->assertNotNull($dataset);
    assert($dataset instanceof DatasetInterface);
    $this->assertEquals('Initial state', $dataset->getStatusLabel());
    return $dataset;
  }

  /**
   * Asserts admin can reprocess dataset.
   *
   * @param \Drupal\data_pipelines\Entity\DatasetInterface $dataset
   *   Dataset to reprocess.
   */
  protected function assertAdminCanReprocessDataset(DatasetInterface $dataset): void {
    $this->drupalGet($dataset->toUrl('process-form'));
    $this->submitForm([], 'Confirm');
    $this->assertSession()->pageTextContains(sprintf('Dataset %s was successfully processed.', $dataset->label()));
    $this->assertStringContainsString(Url::fromRoute('entity.data_pipelines.collection')->toString(), $this->getSession()->getCurrentUrl());
  }

  /**
   * Asserts admin can view dataset logs.
   *
   * @param \Drupal\data_pipelines\Entity\DatasetInterface $dataset
   *   Dataset.
   */
  protected function assertAdminCanViewDatasetLogs(DatasetInterface $dataset): void {
    $message = $this->getRandomGenerator()->sentences(10);
    $dataset->addLogMessage($message);
    $this->drupalGet($dataset->toUrl());
    $this->assertSession()->pageTextContains($message);
  }

}
