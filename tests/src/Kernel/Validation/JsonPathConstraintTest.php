<?php

declare(strict_types=1);

namespace Drupal\Tests\data_pipelines\Kernel\Validation;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Tests\data_pipelines\Kernel\DatasetKernelTestBase;
use Drupal\data_pipelines\Entity\Dataset;
use Drupal\data_pipelines\Plugin\Validation\Constraint\JsonPathConstraint;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * A class to test the json path constraint.
 *
 * @group data_pipelines
 */
class JsonPathConstraintTest extends DatasetKernelTestBase {

  /**
   * A valid json path.
   *
   * @var string
   */
  const VALID_JSON_PATH = '$..data';

  /**
   * An invalid json path.
   *
   * @var string
   */
  const INVALID_JSON_PATH = 'data..$';
  use ProphecyTrait;

  /**
   * A method to test the validation.
   */
  public function testValidation(): void {
    $this->assertValidJsonPath();
    $this->assertInvalidJsonPath();
  }

  /**
   * A method to create a dataset.
   *
   * @return \Drupal\data_pipelines\Entity\Dataset
   *   The dataset.
   */
  private function createDataset(): Dataset {
    $destination = $this->createTestMemoryDestination();
    return Dataset::create([
      'source' => 'json:text',
      'destinations' => [$destination],
      'name' => $this->randomMachineName(),
      'machine_name' => mb_strtolower($this->randomMachineName()),
      'pipeline' => 'test_pipeline_1',
    ]);
  }

  /**
   * A method to assert a valid json path.
   */
  private function assertValidJsonPath(): void {
    $dataset = $this->createDataset();
    $dataset->json_path_to_data = self::VALID_JSON_PATH;
    $errors = $dataset->validate();
    $this->assertCount(0, $errors);
  }

  /**
   * A method to assert an invalid json path.
   */
  private function assertInvalidJsonPath(): void {
    $dataset = $this->createDataset();
    $dataset->json_path_to_data = self::INVALID_JSON_PATH;
    $errors = $dataset->validate();
    /** @var \Drupal\data_pipelines\Plugin\Validation\Constraint\JsonPathConstraint $constraint */
    $constraint = $this->prophesize(JsonPathConstraint::class)->reveal();
    $this->assertCount(1, $errors);
    // @codingStandardsIgnoreStart
    $this->assertEquals((string) new FormattableMarkup($constraint->message, ['@path' => self::INVALID_JSON_PATH]), (string) $errors[0]->getMessage());
    // @codingStandardsIgnoreEnd
  }

}
