<?php

declare(strict_types=1);

namespace Drupal\Tests\data_pipelines\Kernel;

use Drupal\Core\Access\AccessResult;
use Drupal\data_pipelines\Entity\Dataset;

/**
 * Defines a class for testing the dataset access handler.
 *
 * @group data_pipelines
 *
 * @covers \Drupal\data_pipelines\EntityHandlers\DatasetAccessHandler
 */
class DatasetAccessHandlerTest extends DatasetKernelTestBase {

  /**
   * Tests access denied to delete unsaved.
   */
  public function testAccessDeniedDeleteUnsaved(): void {
    $dataset = Dataset::create([
      'source' => 'csv:file',
      'name' => $this->randomMachineName(),
      'machine_name' => mb_strtolower($this->randomMachineName()),
    ]);
    $this->assertFalse($dataset->access('delete'));
  }

  /**
   * Tests delete requires permission.
   */
  public function testDeletePermission(): void {
    $dataset = Dataset::create([
      'source' => 'csv:file',
      'name' => $this->randomMachineName(),
      'machine_name' => mb_strtolower($this->randomMachineName()),
    ]);
    $dataset->save();
    $this->assertFalse($dataset->access('delete'));
    $this->setCurrentUser($this->createUser(['data_pipelines delete']));
    $this->assertTrue($dataset->access('delete'));
  }

  /**
   * Tests update requires permission.
   */
  public function testUpdatePermission(): void {
    $dataset = Dataset::create([
      'source' => 'csv:file',
      'name' => $this->randomMachineName(),
      'machine_name' => mb_strtolower($this->randomMachineName()),
    ]);
    $dataset->save();
    $this->assertFalse($dataset->access('update'));
    $this->setCurrentUser($this->createUser(['data_pipelines edit']));
    $this->assertTrue($dataset->access('update'));
  }

  /**
   * Tests viewing requires permission.
   */
  public function testViewPermission(): void {
    $dataset = Dataset::create([
      'source' => 'csv:file',
      'name' => $this->randomMachineName(),
      'machine_name' => mb_strtolower($this->randomMachineName()),
    ]);
    $dataset->save();
    $this->assertFalse($dataset->access('view'));
    $this->assertFalse($dataset->access('view label'));
    $this->setCurrentUser($this->createUser(['data_pipelines list']));
    $this->assertTrue($dataset->access('view'));
    $this->assertTrue($dataset->access('view label'));
  }

  /**
   * Tests create access.
   */
  public function testCreateAccess(): void {
    $handler = \Drupal::entityTypeManager()->getAccessControlHandler('data_pipelines');
    $this->assertFalse($handler->createAccess('csv:file'));
    $this->setCurrentUser($this->createUser(['data_pipelines create']));
    $this->assertTrue($handler->createAccess('csv:file'));
  }

  /**
   * Tests arbitrary access.
   */
  public function testArbitraryOperationAccess(): void {
    $dataset = Dataset::create([
      'source' => 'csv:file',
      'name' => $this->randomMachineName(),
      'machine_name' => mb_strtolower($this->randomMachineName()),
    ]);
    $dataset->save();
    $this->assertEquals(AccessResult::neutral(), $dataset->access('view revision', NULL, TRUE));
    $this->setCurrentUser($this->createUser([
      'data_pipelines edit',
      'data_pipelines list',
      'data_pipelines delete',
    ]));
    $this->assertEquals(AccessResult::neutral(), $dataset->access('view revision', NULL, TRUE));
  }

}
