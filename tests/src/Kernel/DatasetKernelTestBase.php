<?php

declare(strict_types=1);

namespace Drupal\Tests\data_pipelines\Kernel;

use DG\BypassFinals;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\data_pipelines\Traits\DatasetTestTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\data_pipelines\Entity\Dataset;
use Drupal\data_pipelines\Entity\DatasetInterface;
use Drupal\data_pipelines\Entity\Destination;
use Drupal\data_pipelines\Entity\DestinationInterface;

/**
 * Defines a base kernel test for testing dataset functionality.
 *
 * @group data_pipelines
 */
abstract class DatasetKernelTestBase extends KernelTestBase {

  use UserCreationTrait;
  use DatasetTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'options',
    'link',
    'file',
    'entity',
    'data_pipelines',
    'data_pipelines_test',
    'user',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->afterInitialSetup();
    $this->installEntitySchema('user');
    $this->installEntitySchema('file');
    $this->installEntitySchema('data_pipelines');
    $this->installSchema('file', ['file_usage']);
    $this->setUpCurrentUser();
    BypassFinals::enable(FALSE);
  }

  /**
   * React to initial setup before entity-types are installed.
   */
  protected function afterInitialSetup(): void {
    // Nil-op.
  }

  /**
   * Creates a test dataset.
   *
   * @param array $values
   *   Field values.
   */
  protected function createTestDataset(array $values = []): DatasetInterface {
    if (!isset($values['destinations'])) {
      $destinationId = $values['destinationId'] ?? $this->randomMachineName();
      $values['destinations'] = [$this->createTestMemoryDestination(['id' => $destinationId])];
    }

    $name = $this->randomMachineName();
    $csv = <<<CSV
should_we,firstname,lastname
Y,joe,bloggs
N,betty,bloggs
CSV;
    $dataset = Dataset::create($values + [
      'source' => 'csv:text',
      'pipeline' => 'test_pipeline_1',
      'name' => $name,
      'machine_name' => mb_strtolower($name),
      'csv_text' => $csv,
    ]);
    $dataset->save();
    return $dataset;
  }

  /**
   * Creates a test memory destination.
   *
   * @return \Drupal\data_pipelines\Entity\DestinationInterface
   *   The test destination.
   */
  protected function createTestMemoryDestination(array $values = []): DestinationInterface {
    $destinationId = $values['destinationId'] ?? $this->randomMachineName();
    $destination = Destination::create($values + [
      'id' => $destinationId,
      'label' => 'Test destination',
      'destination' => 'state',
    ]);
    $destination->save();
    return $destination;
  }

  /**
   * Creates a test file destination.
   *
   * @return \Drupal\data_pipelines\Entity\DestinationInterface
   *   The test destination.
   */
  protected function createTestFileDestination(array $values = []): DestinationInterface {
    $destinationId = $values['destinationId'] ?? $this->randomMachineName();
    $destination = Destination::create($values + [
      'id' => $destinationId,
      'label' => 'Test destination',
      'destination' => 'file_json',
      'destinationSettings' => [
        'scheme' => 'public',
        'dir' => '',
      ],
    ]);
    $destination->save();
    return $destination;
  }

}
