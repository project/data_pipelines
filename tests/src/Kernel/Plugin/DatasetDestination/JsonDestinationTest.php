<?php

namespace Drupal\Tests\data_pipelines\Unit\Plugin\DatasetDestination;

use Drupal\Tests\data_pipelines\Kernel\DatasetKernelTestBase;
use Drupal\data_pipelines\Plugin\DatasetDestination\JsonDestination;

/**
 * Tests the JSON destination.
 *
 * @group data_pipelines
 * @coversDefaultClass \Drupal\data_pipelines\Plugin\DatasetDestination\JsonDestination
 */
class JsonDestinationTest extends DatasetKernelTestBase {

  /**
   * @covers ::beginProcessing
   * @covers ::processChunk
   * @covers ::deleteDataSet
   */
  public function testJsonDestinationCreateUpdateDelete() {
    $destinationId = $this->randomMachineName();
    $destination = $this->createTestMemoryDestination(['id' => $destinationId]);
    $dataset = $this->createTestDataset(['destinations' => $destination]);

    $fileSystem = \Drupal::service('file_system');
    $streamWrapperManager = \Drupal::service('stream_wrapper_manager');
    $configuration = [
      'scheme' => 'public',
      'dir' => '',
    ];
    $plugin_id = 'foo';
    $plugin_definition = [];
    $destinationPlugin = new JsonDestination($configuration, $plugin_id, $plugin_definition, $fileSystem, $streamWrapperManager);
    $file_path = $destinationPlugin->getFilePath($dataset);

    // Create.
    $this->assertTrue($destinationPlugin->beginProcessing($dataset));
    $this->assertTrue($destinationPlugin->processChunk($dataset, iterator_to_array($dataset->getDataIterator())));
    $this->assertEquals('[{"should_we":true,"full_name":"bloggs, joe"},{"should_we":false,"full_name":"bloggs, betty"}]', file_get_contents($file_path));

    // Update.
    $dataset->csv_text[0]->value .= "\nY,robin,scherbatsky";
    $dataset->save();
    $this->assertTrue($destinationPlugin->beginProcessing($dataset));
    $this->assertTrue($destinationPlugin->processChunk($dataset, iterator_to_array($dataset->getDataIterator())));
    $this->assertEquals('[{"should_we":true,"full_name":"bloggs, joe"},{"should_we":false,"full_name":"bloggs, betty"},{"should_we":true,"full_name":"scherbatsky, robin"}]', file_get_contents($file_path));

    // Delete.
    $this->assertTrue($destinationPlugin->deleteDataSet($dataset, $destination));
    $this->assertFileDoesNotExist($file_path);
  }

}
