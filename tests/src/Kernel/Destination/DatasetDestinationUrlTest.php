<?php

declare(strict_types=1);

namespace Drupal\Tests\data_pipelines\Kernel\Destination;

use Drupal\Core\Url;
use Drupal\Tests\data_pipelines\Kernel\DatasetKernelTestBase;
use Drupal\data_pipelines\Plugin\DatasetDestination\JsonDestination;

/**
 * Tests the getFileUrl of the FileDestinationBase class.
 *
 * @group data_pipelines
 *
 * @see \Drupal\data_pipelines\Plugin\DatasetDestination\FileDestinationBase
 */
class DatasetDestinationUrlTest extends DatasetKernelTestBase {

  /**
   * Tests that filemtime is added to getUrl method.
   */
  public function testFileUrlForDestination(): void {
    $destinationId = $this->randomMachineName();
    $destination = $this->createTestMemoryDestination(['id' => $destinationId]);
    $dataset = $this->createTestDataset(['destinations' => $destination]);

    $fileSystem = \Drupal::service('file_system');
    $streamWrapperManager = \Drupal::service('stream_wrapper_manager');
    $configuration = [
      'scheme' => 'public',
      'dir' => '',
    ];
    $plugin_id = 'foo';
    $plugin_definition = [];
    $destinationPlugin = new JsonDestination($configuration, $plugin_id, $plugin_definition, $fileSystem, $streamWrapperManager);
    $file_path = $destinationPlugin->getFilePath($dataset);

    // Create.
    $this->assertTrue($destinationPlugin->beginProcessing($dataset));
    $this->assertTrue($destinationPlugin->processChunk($dataset, iterator_to_array($dataset->getDataIterator())));
    $this->assertEquals('[{"should_we":true,"full_name":"bloggs, joe"},{"should_we":false,"full_name":"bloggs, betty"}]', file_get_contents($file_path));

    $url = $destinationPlugin->getFileUrl($dataset);
    assert($url instanceof Url);
    $this->assertArrayHasKey('t', $url->getOption('query'));
    $timestamp1 = $url->getOption('query')['t'];
    $this->assertEquals($timestamp1, filemtime(\Drupal::service('file_system')->realpath($destinationPlugin->getFilePath($dataset))));

    sleep(2);

    // Update and test the timestamp again.
    $dataset->csv_text[0]->value .= "\nY,robin,scherbatsky";
    $dataset->save();
    $this->assertTrue($destinationPlugin->beginProcessing($dataset));
    $this->assertTrue($destinationPlugin->processChunk($dataset, iterator_to_array($dataset->getDataIterator())));
    $this->assertEquals('[{"should_we":true,"full_name":"bloggs, joe"},{"should_we":false,"full_name":"bloggs, betty"},{"should_we":true,"full_name":"scherbatsky, robin"}]', file_get_contents($file_path));

    $url = $destinationPlugin->getFileUrl($dataset);
    $timestamp2 = $url->getOption('query')['t'];
    $this->assertEquals($timestamp2, filemtime(\Drupal::service('file_system')->realpath($destinationPlugin->getFilePath($dataset))));
    $this->assertNotEquals($timestamp2, $timestamp1);
  }

}
