<?php

declare(strict_types=1);

namespace Drupal\Tests\data_pipelines\Kernel\Destination;

use Drupal\Tests\data_pipelines\Kernel\DatasetKernelTestBase;
use Drupal\data_pipelines\Destination\DestinationDeleteRequest;
use Drupal\data_pipelines\Destination\DestinationSaveRequest;

/**
 * Defines a class for testing indexing request.
 *
 * @group data_pipelines
 * @group legacy
 *
 * @covers \Drupal\data_pipelines\Destination\DestinationRequest
 */
class DestinationRequestTest extends DatasetKernelTestBase {

  /**
   * Tests for dataset.
   */
  public function testSaveDataset() {
    $dataset = $this->createTestDataset();
    $request = new DestinationSaveRequest($dataset->getMachineName(), $dataset->label());
    $this->assertEquals($dataset->getMachineName(), $request->getMachineName());
    $this->assertEquals($dataset->label(), $request->getLabel());
  }

  /**
   * Tests for deletion.
   */
  public function testDeleteDataset() {
    $dataset = $this->createTestDataset();
    $request = new DestinationDeleteRequest($dataset->getMachineName(), $dataset->label());
    $this->assertEquals($dataset->getMachineName(), $request->getMachineName());
    $this->assertEquals($dataset->label(), $request->getLabel());
  }

}
