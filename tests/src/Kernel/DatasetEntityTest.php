<?php

declare(strict_types=1);

namespace Drupal\Tests\data_pipelines\Kernel;

use Drupal\data_pipelines\Entity\Dataset;
use Drupal\data_pipelines\Entity\DatasetInterface;

/**
 * Defines a class for testing the dataset entity.
 *
 * @group data_pipelines
 *
 * @covers \Drupal\data_pipelines\Entity\Dataset
 */
class DatasetEntityTest extends DatasetKernelTestBase {

  /**
   * Tests get plugins.
   */
  public function testGetPlugins() {
    $dataset = Dataset::create([
      'source' => 'csv:file',
      'name' => $this->randomMachineName(),
      'machine_name' => mb_strtolower($this->randomMachineName()),
    ]);
    $this->assertEquals([
      'test_pipeline_1' => 'Test pipeline 1',
      'invalid_pipeline_1' => 'Invalid pipeline 1',
      'invalid_pipeline_2' => 'Pipeline with record transform for fields and view versa',
      'pipeline_with_no_validation' => 'No validation',
      'pipeline_with_skip_transform' => 'Pipeline with skip transform',
      'pipeline_with_skip_transform_not_equals' => 'Pipeline with skip transform (Not equals)',
    ], Dataset::getPipelines($dataset->getFieldDefinition('pipeline'), $dataset));
  }

  /**
   * Tests batch size.
   */
  public function testBatchSize(): void {
    $dataset = Dataset::create([
      'source' => 'csv:file',
      'name' => $this->randomMachineName(),
      'machine_name' => mb_strtolower($this->randomMachineName()),
    ]);
    $this->assertEquals(1000, $dataset->getBatchSize());
    $dataset = Dataset::create([
      'source' => 'csv:file',
      'name' => $this->randomMachineName(),
      'batch_size' => 10000,
      'machine_name' => mb_strtolower($this->randomMachineName()),
    ]);
    $this->assertEquals(10000, $dataset->getBatchSize());
  }

  /**
   * Tests invalid values.
   */
  public function testGetInvalidValuesHandling(): void {
    $dataset = Dataset::create([
      'source' => 'csv:file',
      'name' => $this->randomMachineName(),
      'machine_name' => mb_strtolower($this->randomMachineName()),
    ]);
    $this->assertEquals(DatasetInterface::INVALID_RETAIN, $dataset->getInvalidValuesHandling());
    $dataset = Dataset::create([
      'source' => 'csv:file',
      'name' => $this->randomMachineName(),
      'invalid_values' => DatasetInterface::INVALID_REMOVE,
      'machine_name' => mb_strtolower($this->randomMachineName()),
    ]);
    $this->assertEquals(DatasetInterface::INVALID_REMOVE, $dataset->getInvalidValuesHandling());
  }

  /**
   * Tests set setting status.
   */
  public function testStatus() {
    $dataset = Dataset::create([
      'source' => 'csv:file',
      'name' => $this->randomMachineName(),
      'machine_name' => mb_strtolower($this->randomMachineName()),
    ]);
    $this->assertEquals('Initial state', $dataset->getStatusLabel());
    $dataset->setPendingValidation();
    $this->assertTrue($dataset->isPendingValidation());
    $dataset->setPendingProcessing();
    $this->assertTrue($dataset->isPendingProcessing());
    $dataset->setProcessingComplete();
    $this->assertTrue($dataset->isProcessed());
  }

  /**
   * Tests logging.
   */
  public function testLogging() {
    $dataset = Dataset::create([
      'source' => 'csv:file',
      'name' => $this->randomMachineName(),
      'machine_name' => mb_strtolower($this->randomMachineName()),
    ]);
    $this->assertEquals([], $dataset->getLogs());
    $message1 = $this->randomMachineName();
    $message2 = $this->randomMachineName();
    $dataset->addLogMessage($message1);
    $this->assertEquals([$message1], $dataset->getLogs());
    $dataset->addLogMessage($message2, FALSE);
    $dataset = $this->reloadDataset($dataset);
    $this->assertEquals([$message1], $dataset->getLogs());
    $dataset->addLogMessage($message2, FALSE);
    $dataset->save();
    $dataset = $this->reloadDataset($dataset);
    $this->assertEquals([$message1, $message2], $dataset->getLogs());
    $dataset->setPendingProcessing();
    $this->assertTrue($dataset->isPendingProcessing());
    $dataset->resetLogs(FALSE);
    $this->assertFalse($dataset->isPendingProcessing());
    $this->assertEquals([], $dataset->getLogs());
    $dataset = $this->reloadDataset($dataset);
    $this->assertEquals([$message1, $message2], $dataset->getLogs());
    $dataset->resetLogs();
    $this->assertEquals([], $dataset->getLogs());
    $dataset = $this->reloadDataset($dataset);
    $dataset->resetLogs();
  }

  /**
   * Tests getting source/pipeline labels.
   */
  public function testSourcePipelineLabels() {
    $dataset = $this->createTestDataset();
    $this->assertEquals('CSV Text', $dataset->getSourceLabel());
    $this->assertEquals('Test pipeline 1', $dataset->getPipelineLabel());
    $this->assertEquals('test_pipeline_1', $dataset->getPipelineId());
    $dataset = $this->createTestDataset([
      'pipeline' => 'does_not_exist',
    ]);
    $this->assertEquals('does_not_exist', $dataset->getPipelineLabel());
    $this->assertEquals('does_not_exist', $dataset->getPipelineId());
  }

  /**
   * Tests multiple destinations.
   */
  public function testMultipleDestinations() {
    $destinations[] = $this->createTestMemoryDestination();
    $destinations[] = $this->createTestFileDestination();
    $dataset = $this->createTestDataset(['destinations' => $destinations]);
    $this->assertCount(2, $dataset->getDestinations());
  }

}
