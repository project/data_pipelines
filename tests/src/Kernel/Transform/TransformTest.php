<?php

declare(strict_types=1);

namespace Drupal\Tests\data_pipelines\Kernel\Transform;

use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Logger\RfcLoggerTrait;
use Drupal\Tests\data_pipelines\Kernel\DatasetKernelTestBase;
use Drupal\data_pipelines\DatasetData;
use Drupal\data_pipelines\Entity\Dataset;
use Psr\Log\LoggerInterface;

/**
 * Defines a class for testing transform functionality.
 *
 * @group data_pipelines
 *
 * @covers \Drupal\data_pipelines\DatasetPipelineBase
 * @covers \Drupal\data_pipelines\Plugin\DatasetTransform\Concat
 * @covers \Drupal\data_pipelines\Plugin\DatasetTransform\MapValues
 * @covers \Drupal\data_pipelines\Entity\Dataset
 * @covers \Drupal\data_pipelines\Transform\TransformPluginBase
 * @covers \Drupal\data_pipelines\Transform\DatasetTransformPluginManager
 * @covers \Drupal\data_pipelines\DatasetPipelinePluginManager
 * @covers \Drupal\data_pipelines\TransformValidDataIterator
 */
class TransformTest extends DatasetKernelTestBase implements LoggerInterface {

  use RfcLoggerTrait;

  /**
   * Log.
   *
   * @var array
   */
  protected $log = [];

  /**
   * Tests transform.
   */
  public function testTransform() {
    $file = $this->getTestFile(dirname(__DIR__, 2) . '/fixtures/test-pipeline-1.csv');
    $dataset = Dataset::create([
      'source' => 'csv:file',
      'name' => $this->randomMachineName(),
      'machine_name' => mb_strtolower($this->randomMachineName()),
      'pipeline' => 'test_pipeline_1',
      'csv_file' => $file,
    ]);
    $data = iterator_to_array($dataset->getDataIterator());
    $this->assertCount(2, $data);
    $this->assertEquals([
      new DatasetData([
        'should_we' => TRUE,
        'full_name' => 'bloggs, joe',
      ]),
      new DatasetData([
        'should_we' => FALSE,
        'full_name' => 'bloggs, betty',
      ]),
    ], $data);
    // Test with seek.
    $data = iterator_to_array($dataset->getDataIterator(1));
    $this->assertCount(1, $data);
    $this->assertEquals([
      1 => new DatasetData([
        'should_we' => FALSE,
        'full_name' => 'bloggs, betty',
      ]),
    ], $data);
  }

  /**
   * Test skip transform.
   */
  public function testSkipTransform(): void {
    $file = $this->getTestFile(dirname(__DIR__, 2) . '/fixtures/test-pipeline-1.csv');
    // Skip with equals.
    $dataset = Dataset::create([
      'source' => 'csv:file',
      'name' => $this->randomMachineName(),
      'machine_name' => mb_strtolower($this->randomMachineName()),
      'pipeline' => 'pipeline_with_skip_transform',
      'csv_file' => $file,
    ]);
    $iterator = $dataset->getDataIterator();
    $data = iterator_to_array($iterator);
    $this->assertCount(1, $data);
    $this->assertEquals([
      1 => new DatasetData([
        'should_we' => FALSE,
        'full_name' => 'bloggs, betty',
      ]),
    ], $data);
    $logs = $dataset->getLogs();
    $this->assertCount(1, $logs);
    $expected_logs = [
      'Skip in row 1. The value of the field "full_name" is equal to the value "bloggs, joe".',
    ];
    $this->assertEquals($expected_logs, $logs);
    $this->assertEquals([0], $iterator->getInvalidDeltas());

    // Skip with not equals.
    $dataset = Dataset::create([
      'source' => 'csv:file',
      'name' => $this->randomMachineName(),
      'machine_name' => mb_strtolower($this->randomMachineName()),
      'pipeline' => 'pipeline_with_skip_transform_not_equals',
      'csv_file' => $file,
    ]);
    $iterator = $dataset->getDataIterator();
    $data = iterator_to_array($iterator);
    $this->assertCount(1, $data);
    $this->assertEquals([
      0 => new DatasetData([
        'should_we' => TRUE,
        'full_name' => 'bloggs, joe',
      ]),
    ], $data);
    $logs = $dataset->getLogs();
    $this->assertCount(1, $logs);
    $expected_logs = [
      'Skip in row 2. The value of the field "full_name" is not equal to the value "bloggs, joe".',
    ];
    $this->assertEquals($expected_logs, $logs);
    $this->assertEquals([1], $iterator->getInvalidDeltas());
  }

  /**
   * Tests only the valid data is transformed and validation errors are logged.
   */
  public function testInvalidDataTransform() {
    $file = $this->getTestFile(dirname(__DIR__, 2) . '/fixtures/test-pipeline-1-invalid.csv');
    $dataset = Dataset::create([
      'source' => 'csv:file',
      'name' => $this->randomMachineName(),
      'machine_name' => mb_strtolower($this->randomMachineName()),
      'pipeline' => 'test_pipeline_1',
      'csv_file' => $file,
    ]);
    $iterator = $dataset->getDataIterator();
    $data = iterator_to_array($iterator);
    $this->assertCount(1, $data);
    $this->assertEquals([
      new DatasetData([
        'should_we' => TRUE,
        'full_name' => ', name',
      ]),
    ], array_values($data));
    $expected = [
      'Validation error in row 1: Firstname should be at least 3 characters',
      'Validation error in row 2: Firstname is required',
    ];
    $d10 = version_compare(\Drupal::VERSION, '10', '>');
    if ($d10) {
      $expected[] = 'Validation error in row 2: Firstname should be at least 3 characters';
    }
    $this->assertEquals($expected, $dataset->getLogs());
    $this->assertEquals([0, 1], $iterator->getInvalidDeltas());
  }

  /**
   * Tests pipeline with invalid transform.
   */
  public function testInvalidTransform() {
    $this->container->get('logger.factory')->addLogger($this);
    $file = $this->getTestFile(dirname(__DIR__, 2) . '/fixtures/test-pipeline-1.csv');
    $dataset = Dataset::create([
      'source' => 'csv:file',
      'name' => $this->randomMachineName(),
      'machine_name' => mb_strtolower($this->randomMachineName()),
      'pipeline' => 'invalid_pipeline_1',
      'csv_file' => $file,
    ]);
    iterator_to_array($dataset->getDataIterator());
    $this->assertNotEmpty($this->log);
    [$message] = reset($this->log[RfcLogLevel::ERROR]);
    $this->assertEquals('Invalid plugin ID no_such_plugin for transform.', $message);
    [$message] = reset($this->log[RfcLogLevel::WARNING]);
    $this->assertEquals('Missing plugin ID for transform.', $message);
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, $message, array $context = []):void {
    if ($context['channel'] === 'data_pipelines') {
      $this->log[$level][] = [(string) $message, $context];
    }
  }

  /**
   * Tests pipeline with a field plugin for a record and vice versa.
   */
  public function testPluginWithIncorrectRecordAndFieldSupport() {
    $this->container->get('logger.factory')->addLogger($this);
    $file = $this->getTestFile(dirname(__DIR__, 2) . '/fixtures/test-pipeline-1.csv');
    $dataset = Dataset::create([
      'source' => 'csv:file',
      'name' => $this->randomMachineName(),
      'machine_name' => mb_strtolower($this->randomMachineName()),
      'pipeline' => 'invalid_pipeline_2',
      'csv_file' => $file,
    ]);
    $data = iterator_to_array($dataset->getDataIterator());
    $this->assertCount(2, $data);
    // Data should be unchanged.
    $this->assertEquals([
      new DatasetData([
        'should_we' => 'Y',
        'firstname' => 'joe',
        'lastname' => 'bloggs',
      ]),
      new DatasetData([
        'should_we' => 'N',
        'firstname' => 'betty',
        'lastname' => 'bloggs',
      ]),
    ], $data);
    $this->assertNotEmpty($this->log);
    [$message] = reset($this->log[RfcLogLevel::WARNING]);
    $this->assertEquals('Using transform map to transform records is not support.', $message);
    [$message] = next($this->log[RfcLogLevel::WARNING]);
    $this->assertEquals('Using transform concat to transform fields is not support.', $message);
  }

}
