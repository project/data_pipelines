<?php

declare(strict_types=1);

namespace Drupal\Tests\data_pipelines\Kernel;

use Drupal\Core\Entity\Routing\EntityRouteProviderInterface;
use Symfony\Component\Routing\RouteCollection;

/**
 * Defines a class for testing dataset route provider.
 *
 * @group data_pipelines
 *
 * @covers \Drupal\data_pipelines\EntityHandlers\DatasetRouteProvider
 */
class DatasetRouteProviderTest extends DatasetKernelTestBase {

  /**
   * Tests route provider.
   */
  public function testRouteProvider() {
    $providers = \Drupal::entityTypeManager()->getRouteProviders('data_pipelines');
    $collection = new RouteCollection();
    foreach ($providers as $route_provider) {
      assert($route_provider instanceof EntityRouteProviderInterface);
      $collection->addCollection($route_provider->getRoutes(\Drupal::entityTypeManager()->getDefinition('data_pipelines')));
    }
    $process_form = $collection->get('entity.data_pipelines.process_form');
    $this->assertNotEmpty($process_form);
    $this->assertTrue($process_form->getOption('_admin_route'));
    $this->assertNotEmpty($collection->get('entity.data_pipelines.collection'));
    $this->assertNotEmpty($collection->get('entity.data_pipelines.add_page'));
    $this->assertNotEmpty($collection->get('entity.data_pipelines.canonical'));
    $this->assertNotEmpty($collection->get('entity.data_pipelines.edit_form'));
    $this->assertNotEmpty($collection->get('entity.data_pipelines.delete_form'));
  }

}
