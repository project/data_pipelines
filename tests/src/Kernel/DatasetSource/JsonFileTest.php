<?php

declare(strict_types=1);

namespace Drupal\Tests\data_pipelines\Kernel\DatasetSource;

use Drupal\Tests\data_pipelines\Kernel\DatasetKernelTestBase;
use Drupal\data_pipelines\DatasetData;
use Drupal\data_pipelines\Entity\Dataset;
use Drupal\data_pipelines\Source\DatasetSourceInterface;
use Drupal\data_pipelines\Source\DatasetSourcePluginManager;

/**
 * Defines a test for the JsonFile dataset source.
 *
 * @group data_pipelines
 *
 * @covers \Drupal\data_pipelines\Plugin\DatasetSource\JsonSource
 * @covers \Drupal\data_pipelines\Source\Resource\File
 * @covers \Drupal\data_pipelines\Source\DatasetSourcePluginManager
 */
class JsonFileTest extends DatasetKernelTestBase {

  /**
   * Tests extracting data from JSON files.
   */
  public function testExtractDataFromDataSet() {
    $file = $this->getTestFile(dirname(__DIR__, 2) . '/fixtures/test-pipeline-1.json');
    $manager = \Drupal::service('plugin.manager.data_pipelines_source');
    assert($manager instanceof DatasetSourcePluginManager);
    $instance = $manager->createInstance('json:file');
    assert($instance instanceof DatasetSourceInterface);
    $dataset = Dataset::create([
      'name' => $this->randomMachineName(),
      'machine_name' => mb_strtolower($this->randomMachineName()),
      'source' => 'json:file',
      'pipeline' => 'test_pipeline_1',
      'json_file' => $file,
    ]);
    $data = iterator_to_array($instance->extractDataFromDataSet($dataset));
    $this->assertEquals([
      new DatasetData([
        'should_we' => 'Y',
        'firstname' => 'joe',
        'lastname' => 'bloggs',
      ]),
      new DatasetData([
        'should_we' => 'N',
        'firstname' => 'betty',
        'lastname' => 'bloggs',
      ]),
    ], $data);
  }

  /**
   * Tests extracting data from JSON file where data is an object.
   */
  public function testExtractDataFromObjectDataSet() {
    $file = $this->getTestFile(dirname(__DIR__, 2) . '/fixtures/test-pipeline-object.json');
    $manager = \Drupal::service('plugin.manager.data_pipelines_source');
    assert($manager instanceof DatasetSourcePluginManager);
    $instance = $manager->createInstance('json:file');
    assert($instance instanceof DatasetSourceInterface);
    $dataset = Dataset::create([
      'name' => $this->randomMachineName(),
      'machine_name' => mb_strtolower($this->randomMachineName()),
      'source' => 'json:file',
      'pipeline' => 'test_pipeline_1',
      'json_file' => $file,
    ]);
    $data = iterator_to_array($instance->extractDataFromDataSet($dataset));
    $this->assertEquals([new DatasetData([
      'joe' => [
        'should_we' => 'Y',
        'firstname' => 'joe',
        'lastname' => 'bloggs',
      ],
      'betty' => [
        'should_we' => 'N',
        'firstname' => 'betty',
        'lastname' => 'bloggs',
      ],
    ]),
    ], $data);
  }

}
