<?php

declare(strict_types=1);

namespace Drupal\Tests\data_pipelines\Kernel\DatasetSource;

use Drupal\Tests\data_pipelines\Kernel\DatasetKernelTestBase;
use Drupal\data_pipelines\DatasetData;
use Drupal\data_pipelines\Entity\Dataset;
use Drupal\data_pipelines\Source\DatasetSourceInterface;
use Drupal\data_pipelines\Source\DatasetSourcePluginManager;

/**
 * Defines a test for the CsvFile dataset source.
 *
 * @group data_pipelines
 *
 * @covers \Drupal\data_pipelines\Plugin\DatasetSource\CsvSource
 * @covers \Drupal\data_pipelines\Source\Resource\File
 * @covers \Drupal\data_pipelines\Source\DatasetSourcePluginManager
 */
class CsvFileTest extends DatasetKernelTestBase {

  /**
   * Tests extracting data from CSV files.
   *
   * @dataProvider extractDataFromDataSetFixtures
   */
  public function testExtractDataFromDataSet(string $fixture) {
    $file = $this->getTestFile(dirname(__DIR__, 2) . '/fixtures/' . $fixture);
    $manager = \Drupal::service('plugin.manager.data_pipelines_source');
    assert($manager instanceof DatasetSourcePluginManager);
    $instance = $manager->createInstance('csv:file');
    assert($instance instanceof DatasetSourceInterface);
    $dataset = Dataset::create([
      'source' => 'csv:file',
      'pipeline' => 'test_pipeline_1',
      'name' => $this->randomMachineName(),
      'machine_name' => mb_strtolower($this->randomMachineName()),
      'csv_file' => $file,
    ]);
    $data = iterator_to_array($instance->extractDataFromDataSet($dataset));
    $this->assertEquals([
      new DatasetData([
        'should_we' => 'Y',
        'firstname' => 'joe',
        'lastname' => 'bloggs',
      ]),
      new DatasetData([
        'should_we' => 'N',
        'firstname' => 'betty',
        'lastname' => 'bloggs',
      ]),
    ], $data);
  }

  /**
   * A method that acts as a data provider for fixtures.
   *
   * @return string[]
   *   The fixtures.
   */
  public static function extractDataFromDataSetFixtures(): array {
    return [
      'plain' => ['test-pipeline-1.csv'],
      'excel' => ['test-pipeline-1-bom.csv'],
    ];
  }

}
