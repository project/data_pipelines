<?php

declare(strict_types=1);

namespace Drupal\Tests\data_pipelines\Kernel\DatasetSource;

use Drupal\Tests\data_pipelines\Kernel\DatasetKernelTestBase;
use Drupal\data_pipelines\DatasetData;
use Drupal\data_pipelines\Entity\Dataset;
use Drupal\data_pipelines\Source\DatasetSourceInterface;
use Drupal\data_pipelines\Source\DatasetSourcePluginManager;

/**
 * Defines a test for the JsonString dataset source.
 *
 * @group data_pipelines
 *
 * @covers \Drupal\data_pipelines\Plugin\DatasetSource\JsonSource
 * @covers \Drupal\data_pipelines\Source\Resource\Text
 * @covers \Drupal\data_pipelines\Source\DatasetSourcePluginManager
 */
class JsonTextTest extends DatasetKernelTestBase {

  /**
   * Tests extracting data from JSON strings.
   */
  public function testExtractDataFromDataSet() {
    $manager = \Drupal::service('plugin.manager.data_pipelines_source');
    assert($manager instanceof DatasetSourcePluginManager);
    $instance = $manager->createInstance('json:text');
    assert($instance instanceof DatasetSourceInterface);
    $json = <<<JSON
[
  {
    "should_we": "Y",
    "firstname": "joe",
    "lastname": "bloggs"
  },
  {
    "should_we": "N",
    "firstname": "betty",
    "lastname": "bloggs"
  }
]
JSON;

    $dataset = Dataset::create([
      'source' => 'json:text',
      'name' => $this->randomMachineName(),
      'machine_name' => mb_strtolower($this->randomMachineName()),
      'pipeline' => 'test_pipeline_1',
      'json_text' => $json,
    ]);
    $data = iterator_to_array($instance->extractDataFromDataSet($dataset));
    $this->assertEquals([
      new DatasetData([
        'should_we' => 'Y',
        'firstname' => 'joe',
        'lastname' => 'bloggs',
      ]),
      new DatasetData([
        'should_we' => 'N',
        'firstname' => 'betty',
        'lastname' => 'bloggs',
      ]),
    ], $data);
  }

}
