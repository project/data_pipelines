<?php

declare(strict_types=1);

namespace Drupal\Tests\data_pipelines\Kernel\DatasetSource;

use Drupal\Tests\data_pipelines\Kernel\DatasetKernelTestBase;
use Drupal\data_pipelines\DatasetData;
use Drupal\data_pipelines\Entity\Dataset;
use Drupal\data_pipelines\Source\DatasetSourceInterface;
use Drupal\data_pipelines\Source\DatasetSourcePluginManager;

/**
 * Defines a test for the CsvString dataset source.
 *
 * @group data_pipelines
 *
 * @covers \Drupal\data_pipelines\Plugin\DatasetSource\CsvSource
 * @covers \Drupal\data_pipelines\Source\Resource\Text
 * @covers \Drupal\data_pipelines\Source\DatasetSourcePluginManager
 */
class CsvTextTest extends DatasetKernelTestBase {

  /**
   * Tests extracting data from CSV strings.
   */
  public function testExtractDataFromDataSet() {
    $manager = \Drupal::service('plugin.manager.data_pipelines_source');
    assert($manager instanceof DatasetSourcePluginManager);
    $instance = $manager->createInstance('csv:text');
    assert($instance instanceof DatasetSourceInterface);
    $csv = <<<CSV
should_we,firstname,lastname
Y,joe,bloggs
N,betty,bloggs
CSV;

    $dataset = Dataset::create([
      'name' => $this->randomMachineName(),
      'machine_name' => mb_strtolower($this->randomMachineName()),
      'source' => 'csv:text',
      'pipeline' => 'test_pipeline_1',
      'csv_text' => $csv,
    ]);
    $data = iterator_to_array($instance->extractDataFromDataSet($dataset));
    $this->assertEquals([
      new DatasetData([
        'should_we' => 'Y',
        'firstname' => 'joe',
        'lastname' => 'bloggs',
      ]),
      new DatasetData([
        'should_we' => 'N',
        'firstname' => 'betty',
        'lastname' => 'bloggs',
      ]),
    ], $data);
  }

}
