<?php

declare(strict_types=1);

namespace Drupal\Tests\data_pipelines\Kernel\DatasetSource;

use Drupal\Tests\data_pipelines\Kernel\DatasetKernelTestBase;
use Drupal\Tests\data_pipelines\Traits\MockHttpClientTrait;
use Drupal\data_pipelines\DatasetData;
use Drupal\data_pipelines\Entity\Dataset;
use Drupal\data_pipelines\Source\DatasetSourceInterface;
use Drupal\data_pipelines\Source\DatasetSourcePluginManager;
use GuzzleHttp\Exception\TransferException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

/**
 * Defines a test for the CsvUri dataset source.
 *
 * @group data_pipelines
 *
 * @covers \Drupal\data_pipelines\Plugin\DatasetSource\CsvSource
 * @covers \Drupal\data_pipelines\Source\Resource\Uri
 * @covers \Drupal\data_pipelines\Source\DatasetSourcePluginManager
 */
class CsvUriTest extends DatasetKernelTestBase {

  use MockHttpClientTrait;

  /**
   * {@inheritdoc}
   */
  protected function afterInitialSetup(): void {
    parent::afterInitialSetup();
    $this->mockClient();
  }

  /**
   * Tests extracting data from remote CSV files.
   */
  public function testExtractDataFromDataSet() {
    $csv = <<<CSV
should_we,firstname,lastname
Y,joe,bloggs
N,betty,bloggs
CSV;
    $this->mockResponses([
      new Response(200, [], $csv),
    ]);
    $manager = \Drupal::service('plugin.manager.data_pipelines_source');
    assert($manager instanceof DatasetSourcePluginManager);
    $instance = $manager->createInstance('csv:uri');
    assert($instance instanceof DatasetSourceInterface);

    $uri = 'http://www.example.com/foo.csv';
    $dataset = Dataset::create([
      'name' => $this->randomMachineName(),
      'machine_name' => mb_strtolower($this->randomMachineName()),
      'source' => 'csv:uri',
      'pipeline' => 'test_pipeline_1',
      'csv_uri' => $uri,
    ]);
    $data = iterator_to_array($instance->extractDataFromDataSet($dataset));
    $expected = [
      new DatasetData([
        'should_we' => 'Y',
        'firstname' => 'joe',
        'lastname' => 'bloggs',
      ]),
      new DatasetData([
        'should_we' => 'N',
        'firstname' => 'betty',
        'lastname' => 'bloggs',
      ]),
    ];
    $this->assertEquals($expected, $data);
    $request = reset($this->history)['request'];
    assert($request instanceof Request);
    $this->assertEquals($uri, (string) $request->getUri());

    // A subsequent request should use cache.
    $data = iterator_to_array($instance->extractDataFromDataSet($dataset));
    $this->assertEquals($expected, $data);
    $this->assertCount(1, $this->history);
  }

  /**
   * Tests extracting data from remote CSV files with a response error.
   */
  public function testRecoversFromResponseError() {
    $this->mockResponses([
      new Response(404, [], 'Not found'),
    ]);
    $manager = \Drupal::service('plugin.manager.data_pipelines_source');
    assert($manager instanceof DatasetSourcePluginManager);
    $instance = $manager->createInstance('csv:uri');
    assert($instance instanceof DatasetSourceInterface);

    $uri = 'http://www.example.com/foo.csv';
    $dataset = Dataset::create([
      'source' => 'csv:uri',
      'pipeline' => 'test_pipeline_1',
      'name' => $this->randomMachineName(),
      'machine_name' => mb_strtolower($this->randomMachineName()),
      'csv_uri' => $uri,
    ]);
    $data = iterator_to_array($instance->extractDataFromDataSet($dataset));
    $this->assertEmpty($data);
    $logs = $dataset->getLogs();
    $this->assertCount(1, $logs);
    $this->assertEquals(sprintf('Error fetching %s: 404 - Not Found', $uri), reset($logs));
  }

  /**
   * Tests extracting data from remote CSV files with other error.
   */
  public function testRecoversFromHttpError() {
    $this->mockResponses([
      new TransferException('Eek'),
    ]);
    $manager = \Drupal::service('plugin.manager.data_pipelines_source');
    assert($manager instanceof DatasetSourcePluginManager);
    $instance = $manager->createInstance('csv:uri');
    assert($instance instanceof DatasetSourceInterface);

    $uri = 'http://www.example.com/foo.csv';
    $dataset = Dataset::create([
      'source' => 'csv:uri',
      'pipeline' => 'test_pipeline_1',
      'name' => $this->randomMachineName(),
      'machine_name' => mb_strtolower($this->randomMachineName()),
      'csv_uri' => $uri,
    ]);
    $data = iterator_to_array($instance->extractDataFromDataSet($dataset));
    $this->assertEmpty($data);
    $logs = $dataset->getLogs();
    $this->assertCount(1, $logs);
    $this->assertEquals(sprintf('Error fetching %s: Eek', $uri), reset($logs));
  }

}
