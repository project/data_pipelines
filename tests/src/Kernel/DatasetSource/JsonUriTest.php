<?php

declare(strict_types=1);

namespace Drupal\Tests\data_pipelines\Kernel\DatasetSource;

use Drupal\Tests\data_pipelines\Kernel\DatasetKernelTestBase;
use Drupal\Tests\data_pipelines\Traits\MockHttpClientTrait;
use Drupal\data_pipelines\DatasetData;
use Drupal\data_pipelines\Entity\Dataset;
use Drupal\data_pipelines\Source\DatasetSourceInterface;
use Drupal\data_pipelines\Source\DatasetSourcePluginManager;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

/**
 * Defines a test for the JsonUri dataset source.
 *
 * @group data_pipelines
 *
 * @covers \Drupal\data_pipelines\Plugin\DatasetSource\JsonSource
 * @covers \Drupal\data_pipelines\Source\Resource\Uri
 * @covers \Drupal\data_pipelines\Source\DatasetSourcePluginManager
 */
class JsonUriTest extends DatasetKernelTestBase {

  use MockHttpClientTrait;

  /**
   * {@inheritdoc}
   */
  protected function afterInitialSetup(): void {
    parent::afterInitialSetup();
    $this->mockClient();
  }

  /**
   * Tests extracting data from remote JSON files.
   */
  public function testExtractDataFromDataSet() {
    $json = <<<JSON
[
  {
    "should_we": "Y",
    "firstname": "joe",
    "lastname": "bloggs"
  },
  {
    "should_we": "N",
    "firstname": "betty",
    "lastname": "bloggs"
  }
]
JSON;
    $this->mockResponses([
      new Response(200, [], $json),
    ]);
    $manager = \Drupal::service('plugin.manager.data_pipelines_source');
    assert($manager instanceof DatasetSourcePluginManager);
    $instance = $manager->createInstance('json:uri');
    assert($instance instanceof DatasetSourceInterface);

    $uri = 'http://www.example.com/foo.json';
    $dataset = Dataset::create([
      'source' => 'json:uri',
      'pipeline' => 'test_pipeline_1',
      'name' => $this->randomMachineName(),
      'machine_name' => mb_strtolower($this->randomMachineName()),
      'json_uri' => $uri,
    ]);
    $data = iterator_to_array($instance->extractDataFromDataSet($dataset));
    $expected = [
      new DatasetData([
        'should_we' => 'Y',
        'firstname' => 'joe',
        'lastname' => 'bloggs',
      ]),
      new DatasetData([
        'should_we' => 'N',
        'firstname' => 'betty',
        'lastname' => 'bloggs',
      ]),
    ];
    $this->assertEquals($expected, $data);
    $request = reset($this->history)['request'];
    assert($request instanceof Request);
    $this->assertEquals($uri, (string) $request->getUri());

    // A subsequent request should use cache.
    $data = iterator_to_array($instance->extractDataFromDataSet($dataset));
    $this->assertEquals($expected, $data);
    $this->assertCount(1, $this->history);
  }

}
