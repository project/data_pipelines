<?php

declare(strict_types=1);

namespace Drupal\Tests\data_pipelines\Traits;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\StreamWrapper\PublicStream;
use Drupal\data_pipelines\Entity\DatasetInterface;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;

/**
 * Helper functions for Dataset tests.
 */
trait DatasetTestTrait {

  /**
   * Gets a file entity for the given file path.
   *
   * @param string $file_path
   *   File path.
   *
   * @return \Drupal\file\FileInterface
   *   File entity.
   */
  protected function getTestFile(string $file_path): FileInterface {
    $file_system = \Drupal::service('file_system');
    assert($file_system instanceof FileSystemInterface);
    $uri = $file_system->copy($file_path, PublicStream::basePath());
    $file = File::create([
      'uri' => $uri,
      'status' => FileInterface::STATUS_PERMANENT,
      'uid' => \Drupal::currentUser()->id(),
    ]);
    $file->save();
    return $file;
  }

  /**
   * Reloads dataset.
   *
   * @param \Drupal\data_pipelines\Entity\DatasetInterface $dataset
   *   Dataset to reload.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   Reloaded dataset if it still exists.
   */
  protected function reloadDataset(DatasetInterface $dataset): ?DatasetInterface {
    return \Drupal::entityTypeManager()
      ->getStorage('data_pipelines')
      ->loadUnchanged($dataset->id());
  }

}
